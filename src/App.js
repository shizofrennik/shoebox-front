import React, { Component } from 'react';

import { BrowserRouter as Router } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';

import Layout from './views/layouts';
import configureStore from './state/store';

const reduxStore = configureStore({});

class App extends Component {
	render() {
		return (
			<ReduxProvider store={reduxStore}>
				<Router>
					<Layout />
				</Router>
			</ReduxProvider>
		);
	}
}

export default App;
