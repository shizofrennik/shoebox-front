import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Logo from '../../components/Logo/Logo';
import Alert from '../../components/Alert/Alert';
import AvatarIcon from '../../components/Avatar/AvatarIcon';
import ButtonIcon from '../../components/Button/ButtonIcon';
import styles from './LayoutHeader.module.scss';
import Photo1 from '../../../assets/images/icon_avatar-1.png';
import Sidebar from '../../components/Sidebar/Sidebar';

class Layout extends Component {
	constructor() {
		super();
		this.state = {
			sideBarIsShown: false,
		};
	}

	onClickOutside = () => {
    this.setState({ sideBarIsShown: false });
	}

	toggleSidebar = () => {
		this.setState({ sideBarIsShown: !this.state.sideBarIsShown });
	}

	render() {
		const { sideBarIsShown } = this.state;
		const { children } = this.props;
		return (
			<header className={styles.header}>
				<div className={styles.containerHeader}>
					<Logo />
					<div className={styles.menuContainer}>{children}</div>
					<div className={styles.headerControls}>
						<AvatarIcon src={Photo1} width={30} height={30} />
						<Alert active />
						<ButtonIcon btnSent type="menu" handleClick={this.toggleSidebar} />
						{sideBarIsShown ? <Sidebar onClickOutside={this.onClickOutside}/> : null}
					</div>
				</div>
			</header>
		);
	}
}

Layout.propTypes = {
	children: PropTypes.object,
};

export default Layout;

// import React from 'react';
// import { Link } from 'react-router-dom';
// import styles from './LayoutHeader.module.css';
// const Layout = ( ) => (
// 	<header className={styles.container}>
// 		<Link to="/" className={styles.link}>Dashboard</Link>
// 		<Link to="/auth">Auth</Link>
// 	</header>
// );

// export default Layout;
