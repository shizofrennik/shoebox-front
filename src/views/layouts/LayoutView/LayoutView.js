import React from 'react';
import { Route } from 'react-router-dom';
import routes from '../../../routes';
import Navigation from '../../components/Navigation';
import styles from './LayoutView.module.css';
import Header from '../LayoutHeader';
// import Footer from '../LayoutFooter';

const Layout = () => (
	<div>
		<Header>
			<Navigation/>
		</Header>

		<div className={styles.container}>
			{ routes.map( route => (
				<Route key={ route.path } { ...route } />
			) ) }
		</div>
	</div>
);

export default Layout;
