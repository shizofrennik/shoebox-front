import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './Sidebar.module.scss';
import Calendar from '../../../assets/images/icon_calendar.svg';
import Quotes from '../../../assets/images/icon_quotes.svg';
import Tasks from '../../../assets/images/tasks.svg';
import Events from '../../../assets/images/events.svg';
import Contacts from '../../../assets/images/contacts.svg';
import Settings from '../../../assets/images/settings.svg';
import Deals from '../../../assets/images/deals.svg';

class Sidebar extends Component {

	componentDidMount() {
		document.addEventListener('click', this.props.onClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('click', this.props.onClickOutside);
	}

	// eslint-disable-next-line class-methods-use-this
	render() {
		return (
			<div className={styles.contSidebar}>
				<ul className={styles.list}>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Contacts} alt="contacts" />
						</div>
						<NavLink
							to={{
								pathname: '/boxes',
								state: { type: 'contact' }
							}}
							className={styles.link}
						>
								Contacts
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Contacts} alt="company" />
						</div>
						<NavLink
							to={{
								pathname: '/boxes',
								state: { type: 'company' }
							}}
							className={styles.link}
						>
								Company
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Deals} alt="deals" />
						</div>
						<NavLink
							to={{
								pathname: '/boxes',
								state: { type: 'deal' }
							}}
							className={styles.link}
						>
								Deals
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Tasks} alt="tasks" />
						</div>
						<NavLink to="#" className={styles.link}>
								Tasks
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Events} alt="events" />
						</div>
						<NavLink to="#" className={styles.link}>
								Events
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Tasks} alt="users" />
						</div>
						<NavLink to="#" className={styles.link}>
								Users
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Tasks} alt="boxes" />
						</div>
						<NavLink
							to={{
								pathname: '/boxes',
								state: { type: 'all' }
							}}
							className={styles.link}
						>
								Boxes
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Quotes} alt="quotes" />
						</div>
						<NavLink to="#" className={styles.link}>
								Quotes
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Calendar} alt="calendar" />
						</div>
						<NavLink to="#" className={styles.link}>
								Calendar
						</NavLink>
					</li>
					<li className={styles.listItem}>
						<div style={{width: '20px', padding: '0px'}}>
							<img src={Settings} alt="settings" />
						</div>
						<NavLink to="/settings" className={styles.link}>
								Settings
						</NavLink>
					</li>
				</ul>
			</div>
		);
	}
}

Sidebar.propTypes = {
	onClickOutside: PropTypes.func,
};

export default Sidebar;
