import React from 'react';
import PropTypes from 'prop-types';
import styles from './InfoRow.module.scss';

function InfoRow({ headText, valueText, style, short }) {
	return (
		<div style={style}>
			<div className={styles.headText}>{headText}</div>
			<div className={styles.flexCont}>
				<div className={styles.valueText}>
					{ short ? valueText.length > 81 ? `${valueText.substr(0, 81)}...` : valueText : valueText }
				</div>
			</div>
		</div>
	);
}

InfoRow.propTypes = {
	headText: PropTypes.string,
	valueText: PropTypes.string,
	style: PropTypes.object,
	short: PropTypes.bool,
};

export default InfoRow;
