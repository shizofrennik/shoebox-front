import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss'; // Import css modules stylesheet as styles
import IconDelete from '../../../assets/images/icon_delete.svg';

class ButtonDelete extends Component {
	render() {
		const { handleClick, style } = this.props;
		return (
			<button style={style} className={styles.btnDelete} onClick={handleClick}>
				<img src={IconDelete} alt=""/>
			</button>
		);
	}
}

ButtonDelete.propTypes = {
	handleClick: PropTypes.func,
	style: PropTypes.object,
};

export default ButtonDelete;
