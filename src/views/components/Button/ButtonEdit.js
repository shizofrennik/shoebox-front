import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss'; // Import css modules stylesheet as styles
import IconEdit from '../../../assets/images/icon_edit.svg';

class ButtonEdit extends Component {
	onClick = (event) => {
		this.props.handleClick();
		event.stopPropagation();
	}
	render() {
		const { style } = this.props;
		return (
			<button style={style} className={styles.btnEdit} onClick={this.onClick}>
				<img src={IconEdit} alt=""/>
			</button>
		);
	}
}

ButtonEdit.propTypes = {
	handleClick: PropTypes.func,
	style: PropTypes.object,
};

export default ButtonEdit;
