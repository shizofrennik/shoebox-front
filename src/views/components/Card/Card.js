import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Card.module.scss';

class Card extends Component {
	render() {
		const {
			first,
			second,
			third,
			fourth,
			fifth,
			sixth,
			seventh,
			eighth,
			empty,
			cardName,
			children,
		} = this.props;
		const className = classNames(empty ? styles.empty : styles.card, {
			[styles.first]: first,
			[styles.second]: second,
			[styles.third]: third,
			[styles.fourth]: fourth,
			[styles.fifth]: fifth,
			[styles.sixth]: sixth,
			[styles.seventh]: seventh,
			[styles.eighth]: eighth,
		});
		return (
			<div className={className}>
				<div className={first ? styles.cardName : styles.secondaryCardName}>
					{cardName ?
						cardName.length > 10 ?
							cardName.substr(0, 10) + '...'
							: cardName
						: ''
					}
				</div>
				{first ? children : null}
			</div>
		);
	}
}

Card.propTypes = {
	children: PropTypes.object,
	first: PropTypes.bool,
	second: PropTypes.bool,
	third: PropTypes.bool,
	fourth: PropTypes.bool,
	fifth: PropTypes.bool,
	sixth: PropTypes.bool,
	seventh: PropTypes.bool,
	eighth: PropTypes.bool,
	empty: PropTypes.bool,
	cardName: PropTypes.string,
	toggleModal: PropTypes.func,
	data: PropTypes.object,
};

export default Card;