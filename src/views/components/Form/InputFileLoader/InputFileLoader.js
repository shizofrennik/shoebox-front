import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './InputFileLoader.module.scss';
import eclipse from '../../../../assets/images/eclipse.png';
import TitleSection from '../../TitleSection/TitleSection';

class InputFileLoader extends Component {
	state = {
		value: '',
	}

	onChange = event => {
		const file = event.target.files[0];
		let reader = new FileReader();
		// reader.onload = (e) => { this.setState({value: e.target.result}) };
		reader.onload = (e) => {
			this.setState({value: e.target.result});
			// this.props.input.onChange(e.target.result);
		}
  	reader.readAsDataURL(file);
	}

	render() {
		const { input } = this.props;
		return (
      <>
			<div className={styles.contPhoto}>
				<img src={input.value || this.state.value || eclipse} alt="avatar" />
				<div className={styles.upload}>
					<label className={styles.label}>
            Upload
						<input
							name={input.name}
							type={input.type}
							value={input.value}
							onChange={this.onChange}
							className={styles.linkText}
						/>
					</label>
				</div>
			</div>
      <div style={{ margin: '8px auto' }}>
      	<TitleSection text="Your Photo" />
      </div>
      </>
		);
	}
}

InputFileLoader.propTypes = {
	input: PropTypes.object,
};

export default InputFileLoader;
