import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Textarea.module.scss';

class Textarea extends Component {
	render() {
		const { style, labelText, placeholder, input } = this.props;
		return (
			<div className={styles.inputWrapper} style={style}>
				<label>
					<span className={styles.labelText}>{labelText}</span>
					<textarea {...input} className={styles.textarea} placeholder={placeholder} />
				</label>
			</div>
		);
	}
}

Textarea.propTypes = {
	style: PropTypes.object,
	labelText: PropTypes.string,
	placeholder: PropTypes.string,
	input: PropTypes.object,
};

export default Textarea;
