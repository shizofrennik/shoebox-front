import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.scss';

class InputSingle extends Component {
	render() {
		const {
			style,
			labelText,
			placeholder,
			name,
			onChange,
			value,
		} = this.props;
		return (
			<div className={styles.inputWrapper} style={style}>
				<label>
					<span className={styles.labelText}>{labelText}</span>
					<input
						className={ styles.input}
						placeholder={placeholder}
						name={name}
						type="text"
						onChange={onChange}
						value={value}
					/>
				</label>
			</div>
		);
	}
}

InputSingle.propTypes = {
	name: PropTypes.string,
	style: PropTypes.object,
	labelText: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
	value: PropTypes.string,
};

export default InputSingle;