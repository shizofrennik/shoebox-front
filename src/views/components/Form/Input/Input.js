import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.scss';

class Input extends Component {
	render() {
		const {
			style,
			labelText,
			placeholder,
			autoFocus,
			meta,
			input } = this.props;
		return (
			<div className={styles.inputWrapper} style={style}>
				<label>
					<span className={styles.labelText}>{labelText}</span>
					<input
						{...input}
						className={ meta.error && meta.touched ? styles.input_warn : styles.input}
						placeholder={placeholder}
						autoFocus={autoFocus}
					/>
				</label>
			</div>
		);
	}
}

Input.propTypes = {
	name: PropTypes.string,
	style: PropTypes.object,
	labelText: PropTypes.string,
	placeholder: PropTypes.string,
	input: PropTypes.object,
	meta: PropTypes.object,
	autoFocus: PropTypes.bool,
};

export default Input;
