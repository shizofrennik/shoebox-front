import React, { Component } from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import styles from './Dropdown.module.scss';
import { Link } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies

const defaultOptions = [
	{ value: 'businesses-1', label: 'businesses-1' },
	{ value: 'businesses-2', label: 'businesses-2' },
	{ value: 'businesses-3', label: 'businesses-3' },
	{ value: 'businesses-4', label: 'businesses-4' },
	{ value: 'businesses-5', label: 'businesses-5' },
	{ value: 'businesses-6', label: 'businesses-6' },
	{ value: 'businesses-7', label: 'businesses-7' },
	{ value: 'businesses-8', label: 'businesses-8' },
	{ value: 'businesses-9', label: 'businesses-9' },
];

const customStyles = {
	container: style => ({ ...style, width: '100%'}),
	multiValueRemove: () => {
		return { color: 'blue' };
	},
	placeholder: () => {
		return {
			color: '#363F55',
			fontSize: '14px',
		};
	},
	option: (provided, state) => ({
		...provided,
		color: state.isSelected ? '#363F55' : '#363F55',
	}),
};

const customStylesForState = {
	container: style => ({ ...style, maxWidth: '150px', height: '32px' }),
	placeholder: () => {
		return {
			color: '#363F55',
			fontSize: '14px',
			fontWeight: 'bold',
		};
	},
	option: (provided, state) => ({
		...provided,
		color: state.isSelected ? '#363F55' : '#363F55',
	}),
};

class Dropdown extends Component {
	render() {
		const { placeholder, labelText, linksIsActive, style, isMulti, stateName, input, options } = this.props;
		return (
			<div style={style}>
				<label className={styles.labelText}>
					<span className={styles.text}>{labelText}</span>
					<Select
						{ ...input }
						styles={stateName ? customStylesForState : customStyles}
						isMulti={isMulti}
						placeholder={placeholder}
						options={options || defaultOptions}
						backspaceRemovesValue={!isMulti}
						controlShouldRenderValue={!isMulti}
						hideSelectedOptions={!isMulti}
						tabSelectsValue={!isMulti}
						isSearchable
					/>
				</label>
				<div className={styles.contLinks}>
					{(linksIsActive && isMulti && input.value)
						? input.value.map((item, index, arr) => (
							<Link to="#" key={index} className={styles.linkText}>
								{ (arr.length - 1) === index ? `${item.label}` : `${item.label}, `}
							</Link>
						))
						: null}
				</div>
			</div>
		);
	}
}

Dropdown.propTypes = {
	placeholder: PropTypes.string,
	labelText: PropTypes.string,
	linksIsActive: PropTypes.bool,
	style: PropTypes.object,
	isMulti: PropTypes.bool,
	stateName: PropTypes.bool,
	options: PropTypes.array,
	input: PropTypes.object,
};

export default Dropdown;
