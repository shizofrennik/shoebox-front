import React, { Component } from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import styles from './Dropdown.module.scss';
import { Link } from 'react-router-dom';
// eslint-disable-next-line import/no-extraneous-dependencies

const defaultOptions = [
  { value: 'contact', label: 'Contact' },
  { value: 'deal', label: 'Deal' },
  { value: 'company', label: 'Company' },
];

const customStyles = {
  placeholder: () => {
    return {
      color: '#363F55',
      fontSize: '14px',
    };
  },
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? '#363F55' : '#363F55',
  }),
};

const customStylesForState = {
  container: style => ({ ...style, maxWidth: '150px', height: '32px' }),
  placeholder: () => {
    return {
      color: '#363F55',
      fontSize: '14px',
      fontWeight: 'bold',
    };
  },
  singleValue: base => ({
    ...base,
    color: '#363F55',
  }),
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? '#363F55' : '#363F55',
  }),
};

class DropdownSingle extends Component {
  constructor() {
    super();
    this.state = {
      selectedOption: null,
    };
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption });
  };

  render() {
    const {
      placeholder,
      labelText,
      linksIsActive,
      style,
      isMulti,
      stateName,
      options,
      value,
      handleChange,
      // limit,
      isDisabled, } = this.props;
    return (
      <div style={style}>
        <label className={styles.labelText}>
          <span className={styles.text}>{labelText}</span>
          <Select
            styles={stateName ? customStylesForState : customStyles}
            isMulti={isMulti}
            placeholder={placeholder}
            value={value}
            onChange={handleChange}
            options={options || defaultOptions}
            backspaceRemovesValue={!isMulti}
            controlShouldRenderValue={!isMulti}
            hideSelectedOptions={!isMulti}
            tabSelectsValue={!isMulti}
            isSearchable
            isDisabled={isDisabled}
          />
        </label>
        <div className={styles.contLinks}>
          {
            linksIsActive && value
            ? value.map((item, index, arr) => (
                <Link to="#" key={index} className={styles.linkText}>
                  { (arr.length - 1) === index ? `${item.label}` : `${item.label}, `}
                </Link>
              ))
            : null
          }
        </div>
      </div>
    );
  }
}

DropdownSingle.propTypes = {
  placeholder: PropTypes.string,
  labelText: PropTypes.string,
  linksIsActive: PropTypes.bool,
  style: PropTypes.object,
  isMulti: PropTypes.bool,
  stateName: PropTypes.bool,
  // limit: PropTypes.bool,
  options: PropTypes.array,
};

export default DropdownSingle;