import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RadioBtn.module.scss'; // Import css modules stylesheet as styles

class RadioBtn extends Component {

	componentDidUpdate() {
		if(this.props.disabled) {
			this.props.input.onChange('');
		}
	}

	render() {
		const { text, disabled, input } = this.props;
		return (
			<div>
				<label className={styles.wrapperRadio}>
					<input
						{...input}
						type="radio"
						disabled={disabled}
					/>
					<div className={styles.radio} />
					{text}
				</label>
			</div>
		);
	}
}

RadioBtn.propTypes = {
	text: PropTypes.string,
	disabled: PropTypes.bool,
	input: PropTypes.object,
	type: PropTypes.string,
};

export default RadioBtn;
