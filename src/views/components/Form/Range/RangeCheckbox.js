import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputRange from 'react-input-range';
import './range.scss';
import styles from './RangeCheckbox.module.scss';

class RangeCheckbox extends Component {
  state = {
    inputRangeIsActive: this.props.valueCheckbox,
  };

  componentDidUpdate() {
    if(!this.state.inputRangeIsActive) {
      this.props.input.onChange('');
    }
  }

  handleInput = e => {
    this.setState({ inputRangeIsActive: e.target.checked });
  };

  render() {
    const { valueCheckbox, text, input } = this.props;
    const { inputRangeIsActive } = this.state;
    return (
      <div>
        <div className={styles.contFlex}>
          <label className={styles.wrapperCheckbox}>
            <input
              type="checkbox"
              value={valueCheckbox}
              checked={this.state.inputRangeIsActive}
              onChange={this.handleInput}
            />
            <div className={styles.checkbox} />
            {text}
          </label>
          <p className={styles.text}>
            Achieve the goal by <span className={styles.text__value}>{`${input.value}%`}</span>
          </p>
        </div>
        <div className={styles.wrapperInputRange}>
          <InputRange
            disabled={!inputRangeIsActive}
            formatLabel={valueRange => `${valueRange}%`}
            maxValue={100}
            minValue={0}
            {...input}
            value={ (input.value === '') ? 0 : input.value}
          />
        </div>
      </div>
    );
  }
}

RangeCheckbox.propTypes = {
  name: PropTypes.string,
  valueCheckbox: PropTypes.bool,
  text: PropTypes.string,
  input: PropTypes.object,
};

export default RangeCheckbox;
