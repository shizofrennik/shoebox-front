import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputRange from 'react-input-range';
import './range.scss';
import styles from './RangeCheckbox.module.scss';

class RangeCheckboxSingle extends Component {
  state = {
    inputRangeIsActive: false,
    inputRangeValue: '',
  };

  handleCheckbox = e => {
    this.setState({ inputRangeIsActive: e.target.checked });
    this.state.inputRangeIsActive && this.setState({ inputRangeValue: '' })
  };

  handleInputRange = value => {
    this.setState({ inputRangeValue: value })
  }

  render() {
    const { valueCheckbox, text, name } = this.props;
    const { inputRangeIsActive, inputRangeValue } = this.state;
    return (
      <div>
        <div className={styles.contFlex}>
          <label className={styles.wrapperCheckbox}>
            <input
              type="checkbox"
              value={valueCheckbox}
              checked={this.state.inputRangeIsActive}
              onChange={this.handleCheckbox}
            />
            <div className={styles.checkbox} />
            {text}
          </label>
          <p className={styles.text}>
            Achieve the goal by <span className={styles.text__value}>{`${inputRangeValue}%`}</span>
          </p>
        </div>
        <div className={styles.wrapperInputRange}>
          <InputRange
            disabled={!inputRangeIsActive}
            formatLabel={valueRange => `${valueRange}%`}
            maxValue={100}
            minValue={0}
            value={inputRangeValue}
            onChange={this.handleInputRange}
            name={name}
            // onChangeComplete={}
          />
        </div>
      </div>
    );
  }
}

RangeCheckboxSingle.propTypes = {
  name: PropTypes.string,
  valueCheckbox: PropTypes.string,
  text: PropTypes.string,
  input: PropTypes.object,
};

export default RangeCheckboxSingle;
