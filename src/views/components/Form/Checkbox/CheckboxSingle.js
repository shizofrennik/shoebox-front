import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Checkbox.module.scss'; // Import css modules stylesheet as styles

class CheckboxSingle extends Component {
	render() {
		const { text, name, value, handleChange, disabled } = this.props;

		return (
			<label className={styles.wrapperCheckbox}>
				<input type="checkbox" name={name} value={value} onChange={handleChange} disabled={disabled} checked={value}/>
				<div className={styles.checkbox} />
				{text}
			</label>
		);
	}
}

CheckboxSingle.propTypes = {
	text: PropTypes.string,
	name: PropTypes.string,
	value: PropTypes.bool,
	disabled: PropTypes.bool,
	handleChange: PropTypes.func,
	checked: PropTypes.bool,
};

export default CheckboxSingle;
