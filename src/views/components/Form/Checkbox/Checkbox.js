import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Checkbox.module.scss'; // Import css modules stylesheet as styles

class Checkbox extends Component {
	render() {
		const { text, input } = this.props;

		return (
			<label className={styles.wrapperCheckbox}>
				<input {...input} />
				<div className={styles.checkbox} />
				{text}
			</label>
		);
	}
}

Checkbox.propTypes = {
	text: PropTypes.string,
	input: PropTypes.object,
	disabled: PropTypes.bool,
};

export default Checkbox;
