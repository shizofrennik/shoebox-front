import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconCalendar from '../../../../assets/images/icon_calendar.svg';
import styles from './DatePick.module.scss';

class CustomInput extends Component {
	render() {
		const { value, labelText, icon } = this.props;
		return (
			<div>
				<span className={styles.labelText}>{labelText}</span>
				<div className={styles.customInput} onClick={this.props.onClick}>
					{icon ? <img className={styles.icon} src={IconCalendar} alt=""/> : null}
					<span className={styles.inputValue}>{value}</span>
				</div>
			</div>
		);
	}
}

CustomInput.propTypes = {
	value: PropTypes.string,
	onClick: PropTypes.func,
	labelText: PropTypes.string,
	icon: PropTypes.bool,
};

export default CustomInput;
