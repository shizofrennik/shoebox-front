import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import CustomInput from './CustomInput';
import 'react-datepicker/dist/react-datepicker.css';

class DatePick extends Component {
	render() {
		const { labelText, icon, input } = this.props;
		return (
			<DatePicker
				{...input}
				customInput={<CustomInput labelText={labelText} icon={icon} />}
				selected={input.value}
				dateFormat="MMMM d, yyyy"
			/>
		);
	}
}

DatePick.propTypes = {
	placeholder: PropTypes.string,
	labelText: PropTypes.string,
	icon: PropTypes.bool,
	input: PropTypes.object
};

export default DatePick;
