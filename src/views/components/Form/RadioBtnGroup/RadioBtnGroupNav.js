import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RadioBtnGroup.module.scss';

class RadioBtnGroupNav extends Component {
	render() {
		const {
			text,
			style,
			checked,
			name,
			value,
			handleChange } = this.props;
		return (
			<label className={styles.radioLabel}>
				<input
					onChange={handleChange}
					name={name}
					value={value}
					type="radio"
					checked={checked}
				/>
				<div style={style} className={styles.radio}>
					{text}
				</div>
			</label>
		);
	}
}

RadioBtnGroupNav.propTypes = {
	text: PropTypes.string,
	style: PropTypes.object,
	checked: PropTypes.bool,
	handleChange: PropTypes.func,
	name: PropTypes.string,
	value: PropTypes.string,

};

export default RadioBtnGroupNav;
