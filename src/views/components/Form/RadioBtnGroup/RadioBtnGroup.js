import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RadioBtnGroup.module.scss';

class RadioBtnGroup extends Component {
	render() {
		const {
			input,
			text,
			style
		} = this.props;
		return (
			<label className={styles.radioLabel}>
				<input
					{...input}
					// onChange={handleChange}
					// name={name}
					// value={value}
					// type="radio"
					// checked={checked}
				/>
				<div style={style} className={styles.radio}>
					{text}
				</div>
			</label>
		);
	}
}

RadioBtnGroup.propTypes = {
	input: PropTypes.object,
	text: PropTypes.string,
	style: PropTypes.object,
	checked: PropTypes.bool,
	handleChange: PropTypes.func,
	name: PropTypes.string,
	value: PropTypes.string,

};

export default RadioBtnGroup;
