import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import PropTypes from 'prop-types';
import styles from './DropdownCheckbox.module.scss';

const defaultOptions = [
	{ value: 'businesses-1', label: 'businesses-1' },
	{ value: 'businesses-2', label: 'businesses-2' },
	{ value: 'businesses-3', label: 'businesses-3' },
];

const customStyles = {
	placeholder: () => {
		return {
			color: '#363F55',
			fontSize: '14px',
		};
	},
	labelText: () => {
		return {
			color: 'red',
			fontSize: '14px',
		};
	},
	option: (provided, state) => ({
		...provided,
		color: state.isSelected ? '#363F55' : '#363F55',
	}),
};

class DropdownCheckboxSingle extends Component {
	render() {
		const {
			placeholder,
			labelText,
			checkboxText,
			linksIsActive,
			isMulti,
			options,
			// handleChange,
			value,
			name } = this.props;
		return (
			<>
				<label className={styles.wrapperCheckbox}>
					<input type="checkbox" name="" onChange={this.handleCheckbox} disabled checked={value}/>
					<div className={styles.checkbox} />
					{checkboxText}
				</label>
				<label className={styles.labelText}>
					<span className={styles.text}>{labelText}</span>
					<Select
						value={value}
						name={name}
						styles={customStyles}
						isMulti={isMulti}
						isDisabled
						placeholder={placeholder}
						options={options || defaultOptions}
						backspaceRemovesValue={!isMulti}
						controlShouldRenderValue={!isMulti}
						hideSelectedOptions={!isMulti}
						tabSelectsValue={!isMulti}
					/>
				</label>
				<div className={styles.contLinks}>
					{linksIsActive && value
						? value.map((item, index, arr) => (
							<Link to="#" key={index} className={styles.linkText}>
								{ (arr.length - 1) === index ? `${item.label}` : `${item.label}, `}
							</Link>
						))
						: null}
				</div>
			</>
		);
	}
}

DropdownCheckboxSingle.propTypes = {
	placeholder: PropTypes.string,
	labelText: PropTypes.string,
	checkboxText: PropTypes.string,
	linksIsActive: PropTypes.bool,
	isMulti: PropTypes.bool,
	value: PropTypes.array,
	name: PropTypes.string,
};

export default DropdownCheckboxSingle;
