import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import PropTypes from 'prop-types';
import styles from './DropdownCheckbox.module.scss';

const defaultOptions = [
  { value: 'businesses-1', label: 'businesses-1' },
  { value: 'businesses-2', label: 'businesses-2' },
  { value: 'businesses-3', label: 'businesses-3' },
];

const customStyles = {
  placeholder: () => {
    return {
      color: '#363F55',
      fontSize: '14px',
    };
  },
  labelText: () => {
    return {
      color: 'red',
      fontSize: '14px',
    };
  },
  option: (provided, state) => ({
    ...provided,
    color: state.isSelected ? '#363F55' : '#363F55',
  }),
};

class DropdownCheckbox extends Component {
  state = {
    selectedOption: null,
    isDisabled: true,
  };

  handleSelect = selectedOption => {
    this.setState({ selectedOption });
  };

  handleCheckbox = e => {
    this.setState({ isDisabled: !e.target.checked });
    this.props.input.onChange(null);
  };

  render() {
    const { placeholder, labelText, checkboxText, linksIsActive, isMulti, options, input } = this.props;
    const { selectedOption, isDisabled } = this.state;
    return (
      <>
        <label className={styles.wrapperCheckbox}>
          <input type="checkbox" name="" onChange={this.handleCheckbox} />
          <div className={styles.checkbox} />
          {checkboxText}
        </label>
        <label className={styles.labelText}>
          <span className={styles.text}>{labelText}</span>
          <Select
            {...input}
            styles={customStyles}
            isMulti={isMulti}
            isDisabled={isDisabled}
            placeholder={placeholder}
            options={options || defaultOptions}
          />
        </label>
        <div className={styles.contLinks}>
          {linksIsActive && selectedOption
            ? selectedOption.map((item, index) => (
                <Link to="#" key={index} className={styles.linkText}>
                  {`${item.label}, `}
                </Link>
              ))
            : null}
        </div>
      </>
    );
  }
}

DropdownCheckbox.propTypes = {
  placeholder: PropTypes.string,
  labelText: PropTypes.string,
  checkboxText: PropTypes.string,
  linksIsActive: PropTypes.bool,
  isMulti: PropTypes.bool,
};

export default DropdownCheckbox;
