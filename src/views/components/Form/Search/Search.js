import React, { Component } from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import styles from './Search.module.scss';
import Loupe from '../../../../assets/images/loupe.svg';

class Search extends Component {
  constructor() {
    super();
    this.state = {
      inputValue: '',
    };
  }

  handleInput = e => {
    this.setState({ inputValue: e.target.value });
  };

  componentDidUpdate() {
    this.props.handleInput(queryString.stringify({ foo: this.state.inputValue }));
  }

  render() {
    const { style } = this.props;
    const { inputValue } = this.state;
    return (
      <div className={styles.cont} style={style}>
        <label htmlFor="search" style={{ display: 'flex' }}>
          <img src={Loupe} alt="loupe" />
        </label>
        <input
          onChange={this.handleInput}
          id="search"
          className={styles.input}
          type="search"
          value={inputValue}
          name=""
          placeholder="Search"
        />
      </div>
    );
  }
}

Search.propTypes = {
  style: PropTypes.object,
  handleInput: PropTypes.func,
};

export default Search;
