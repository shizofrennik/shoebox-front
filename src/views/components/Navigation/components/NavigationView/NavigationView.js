import React, { Component } from 'react';
import HorizontalScroll from 'react-scroll-horizontal';
import styles from './NavigationView.module.scss';
import { Link } from 'react-router-dom';

class NavigationView extends Component {
  shouldComponentUpdate(nextProps) {
    if(this.props.boxes.length && this.props.boxes[0].id === nextProps.boxes[0].id) {
      return false
    }
    return true
  }

  disableScroll = () => {
    document.body.style.paddingRight = '15px';
    document.body.style.overflow = 'hidden';
  };

  enableScroll = () => {
    document.body.style.paddingRight = '0px';
    document.body.style.overflow = 'scroll';
  };
  render() {
    const { boxes } = this.props;
    console.log(boxes);
    return (
      <div style={{ height: '100%' }} onMouseOver={this.disableScroll} onMouseOut={this.enableScroll}>
      { boxes.length === 0 ? null :
          120 * boxes.length > 720 ?
            (<HorizontalScroll pageLock={false} className={styles.contNav}>
              {
                boxes.map( item => (
                  <div className={styles.navItem} key={item.id}>
                    <Link
                      className={styles.navLink}
                      to={{
                        pathname: `/boxes/${item.id}`,
                        state: {
                          id: item.id,
                          type: item.box_deal ? 'deal' : item.box_company ? 'company' : item.box_contact ? 'contact' : null
                        }}}
                    >
                      { item.box_name }
                    </Link>
                  </div>
                ) )
              }
            </HorizontalScroll>) :
            <div className={styles.contNavWithoutScrolling}>
              {
                boxes.map( item => (
                <div className={styles.navItem} key={item.id}>
                  <Link
                    className={styles.navLink}
                    to={{
                      pathname: `/boxes/${item.id}`,
                      state: {
                        id: item.id,
                        type: item.box_deal ? 'deal' : item.box_company ? 'company' : item.box_contact ? 'contact' : null
                      }}}
                  >
                    { item.box_name }
                  </Link>
                </div>
                ) )
              }
            </div>
      }
      </div>
    );
  }
}

export default NavigationView;
