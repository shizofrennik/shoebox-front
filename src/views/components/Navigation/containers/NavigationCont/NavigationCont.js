// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NavigationView from '../../components/NavigationView';

const mapStateToProps = (state) => {
	return ({
		boxes: state.box.boxes,
	});
};

export default connect(mapStateToProps)(NavigationView);
