import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ButtonEdit from '../Button/ButtonEdit';
import ButtonDelete from '../Button/ButtonDelete';
import styles from './InfoRowMutable.module.scss';

class InfoRowMutable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonsAreVisible: false,
      inputIsVisible: false,
    };
  }

  showButtons = () => {
    this.setState({ buttonsAreVisible: true });
  };

  hideButtons = () => {
    this.setState({ buttonsAreVisible: false });
  };

  showInput = () => {
    this.setState({ inputIsVisible: !this.state.inputIsVisible });
  };

  render() {
    const {
      labelText,
      style,
      input
    } = this.props;
    const {
      buttonsAreVisible,
      inputIsVisible,
    } = this.state;
    return (
      <div style={style} className={styles.mainCont}>
        <div className={styles.headText}>{labelText}</div>
        <div onMouseEnter={this.showButtons} onMouseLeave={this.hideButtons} className={styles.flexCont}>
          {
            inputIsVisible ?
              <input
                autoFocus={true}
                className={styles.input}
                type="text"
                name={input.name}
                value={input.value}
                onChange={input.onChange}
                onBlur={this.showInput}
              /> :
              <div className={styles.valueText}>{ input.value }</div>
          }
          {
            buttonsAreVisible && !inputIsVisible ? (
              <div className={ input.value ? styles.contBtn : styles.contBtnEmptyInput }>
                <ButtonEdit style={{ marginLeft: '17px', marginBottom: '3px' }} handleClick={this.showInput}/>
                <ButtonDelete style={{ marginLeft: '6px', marginBottom: '3px' }}/>
              </div>
            ) : null
          }
        </div>
      </div>
    );
  }
}

InfoRowMutable.propTypes = {
  labelText: PropTypes.string,
  style: PropTypes.object,
  short: PropTypes.bool,
};

export default InfoRowMutable;
