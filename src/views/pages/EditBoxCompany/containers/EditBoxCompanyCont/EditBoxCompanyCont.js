import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EditBoxCompanyView from '../../components/EditBoxCompanyView';
// import { boxActions } from '../../../../../state/ducks/box';
import { boxSelectors } from '../../../../../state/ducks/box';

// const { fetchSearchCards, fetchAddCard, fetchEditCard } = boxActions;
const { getDataOfActiveBox } = boxSelectors;

const mapStateToProps = (state, ownProps) => {
	return ({
		boxData: getDataOfActiveBox(state, ownProps.history.location.state.id),
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		// fetchAuth
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(EditBoxCompanyView);