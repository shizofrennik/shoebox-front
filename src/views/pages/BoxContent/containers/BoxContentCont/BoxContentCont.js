import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { boxActions } from '../../../../../state/ducks/box';
import BoxContentView from '../../components/BoxContentView';
import { boxSelectors } from '../../../../../state/ducks/box';
import { cardsSelectors } from '../../../../../state/ducks/cards';

const { fetchSearchCards, fetchAddCard, fetchEditCard, fetchDeleteBox } = boxActions;
const { getCardsOfActiveBox, getDataOfActiveBox } = boxSelectors;
const { getCards } = cardsSelectors;


const mapStateToProps = (state, ownProps) => {
	return ({
		cards: getCardsOfActiveBox(state, ownProps.history.location.state.id, ownProps.history.location.state.type),
		boxData: getDataOfActiveBox(state, ownProps.history.location.state.id),
		deals: getCards(state, 'deals'),
		contacts: getCards(state, 'contacts'),
		companies: getCards(state, 'companies'),
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchSearchCards,
		fetchAddCard,
		fetchEditCard,
		fetchDeleteBox,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(BoxContentView);
