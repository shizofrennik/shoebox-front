import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from '../Head';
import NewDeal from '../NewDeal';
import MoreInfoDeal from '../MoreInfoDeal';
import Arrows from '../../../../components/Button/Arrows';
import Button from '../../../../components/Button/Button';
import Card from '../../../../components/Card/Card';
import Search from '../../../../components/Form/Search/Search';
import InfoRow from '../../../../components/InfoRow/InfoRow';
import ButtonIcon from '../../../../components/Button/ButtonIcon';
import DropdownCheckboxSingle from '../../../../components/Form/DropdownCheckbox/DropdownCheckboxSingle';
import DropdownSingle from '../../../../components/Form/Dropdown/DropdownSingle';
import styles from './BoxDeal.module.scss';
import TitleSection from '../../../../components/TitleSection/TitleSection';
// import TableAnalitics from '../../../../components/TableAnalitics/TableAnalitics';
// import RowTableAnalitics from '../../../../components/TableAnalitics/RowTableAnalitics';
// import FilterHistory from '../../../../components/History/FilterHistory';
// import FilterTasks from '../../../../components/TasksEvents/FilterTasks';
// import TaskList from '../../../../components/TasksEvents/TaskList';
// import HistoryList from '../../../../components/History/HistoryList';

class BoxDeal extends Component {
	state = {
		moreInfoIsOpen: false,
		newDealIsOpen: false,
		startCard: 0,
	};

	goToTheNextCard = () => {
		this.setState({startCard: this.state.startCard + 1});
	}

	goToThePreviousCard = () => {
		this.setState({startCard: this.state.startCard - 1});
	}

	toggleMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: !this.state.moreInfoIsOpen})
	}

	closeMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: false})
	}

	toggleNewDealModal = () => {
		this.setState({newDealIsOpen: !this.state.newDealIsOpen})
	}

	render() {
		const { cards = [], fetchAddCard, fetchEditCard, boxData, goToPage, companies, contacts } = this.props;
		const { moreInfoIsOpen, newDealIsOpen, startCard } = this.state;
		return (
			<div className={styles.containerContent}>
				<Head
					boxName={boxData.box_name}
					cardCount={cards.length}
					boxType="deal"
					style={{paddingTop: '13px'}}
					editBox={() => goToPage('/editboxdeal', { id: boxData.id })}
				/>
				<Search handleInput={() => {}} style={{marginLeft: '0px', marginTop: '20px', marginBottom: '15px'}}/>
				<TitleSection text="Deal" />
				<div className={styles.flexCont}>
					<Button text="+ Add new deal" handleClick={this.toggleNewDealModal}/>
					<Search handleInput={() => {}} />
					<div
						style={{
							fontFamily: 'Montserrat',
							fontStyle: 'normal',
							fontWeight: 'normal',
							fontSize: '14px',
							lineHeight: '17px',
							color: '#363F55',
						}}
					>
						All { cards.length } cards
					</div>
				</div>
				<div className={styles.cardContainer}>
					{cards.length ?
						<Card cardName={cards[startCard].title} first>
							<div className={styles.flexCont}>
								<div className={styles.column}>
									<InfoRow headText="Deal" valueText={cards[startCard].title ||'Empty'} />
									<InfoRow headText="Deal amount" valueText={cards[startCard].deal_amount ||'Empty'} />
									<InfoRow headText="Probability" valueText={cards[startCard].probability ||'Empty'} />
									<InfoRow headText="Adjusted forecast amount" valueText={cards[startCard].adjusted_forecast_amount ||'Empty'} />
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow headText="Mobile phone" valueText={cards[startCard].mobile_phone || ''} />
										<div style={{ display: 'flex', alignItems: 'flex-end' }}>
											<ButtonIcon style={{ marginLeft: '68px', marginBottom: '-5px' }} btnSent type="call" />
											<ButtonIcon style={{ marginLeft: '5px', marginBottom: '-5px' }} btnSent type="sent" />
										</div>
									</div>
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow headText="Work email" valueText={cards[startCard].work_email || ''} />
										<ButtonIcon style={{ marginBottom: '-5px' }} btnSent type="sent" />
									</div>
								</div>
								<div className={styles.column}>
									<div style={{ marginTop: '10px' }}>
										<DropdownSingle
											labelText="Stage"
											placeholder="No Stage"
											value={cards[startCard].stage}
											isDisabled
										/>
									</div>
									<div style={{ marginTop: '10px' }}>
										<DropdownSingle
											labelText="Source"
											placeholder="No Source"
											value={cards[startCard].source}
											isDisabled
										/>
									</div>
									<div style={{ marginTop: '10px', height: '33%' }}>
										<DropdownSingle
											isDisabled
											value={cards[startCard].contacts}
											placeholder="Contact Name"
											labelText="Associated contact(s)"
											linksIsActive
											isMulti
										/>
									</div>
									<div style={{ marginTop: '10px' }}>
										<DropdownCheckboxSingle
											checkboxText="Added to campaign"
											placeholder="Choose Campaign Name"
											value={cards[startCard].campaigns}
											linksIsActive
											isMulti
										/>
									</div>
								</div>
								<div className={styles.column}>
									<div style={{ marginTop: '10px', height: '33%' }}>
										<DropdownSingle
											isDisabled
											value={cards[startCard].companies}
											placeholder="Company Name"
											labelText="Associated company(ies)"
											linksIsActive
											isMulti />
									</div>
									<div style={{ marginTop: '10px' }}>
										<InfoRow headText="Summary" valueText={cards[0].summary ||''} />
									</div>
									<div style={{ marginTop: '10px' }}>
										<DropdownSingle
											labelText="Add to box"
											value={cards[0].box}
											isDisabled
										/>
									</div>
									<div className={`${styles.flexCont} ${styles.justifySpaceBetween}`} style={{ marginTop: '38px' }}>
										<Button
											style={{ width: 'calc(50% - 5px)' }}
											transparentBorder
											text="More Info"
											handleClick={this.toggleMoreInfoModal}
										/>
										<ButtonIcon style={{ width: 'calc(50% - 5px)' }} btnDrop type="drop" text="Drop back" />
									</div>
								</div>
							</div>
						</Card> : <Card empty/>}
						{ cards.length > 1 && (startCard + 1) < cards.length ? <Card cardName={cards[startCard + 1].title} second /> : null }
						{ cards.length > 2 && (startCard + 2) < cards.length ? <Card cardName={cards[startCard + 2].title} third /> : null }
						{ cards.length > 3 && (startCard + 3) < cards.length ? <Card cardName={cards[startCard + 3].title} fourth /> : null }
						{ cards.length > 4 && (startCard + 4) < cards.length ? <Card cardName={cards[startCard + 4].title} fifth /> : null }
						{ cards.length > 5 && (startCard + 5) < cards.length ? <Card cardName={cards[startCard + 5].title} sixth /> : null }
						{ cards.length > 6 && (startCard + 6) < cards.length ? <Card cardName={cards[startCard + 6].title} seventh /> : null }
						{ cards.length > 7 && (startCard + 7) < cards.length ? <Card cardName={cards[startCard + 7].title} eighth /> : null }
						{ cards.length > 1 ?
					<>
						<div className={[`${styles.contBut} ${styles.contBut_first}`]}>
							<Arrows handleClick={this.goToThePreviousCard} arrowsDisabled={startCard === 0}/>
						</div>
						<div className={[`${styles.contBut} ${styles.contBut_second}`]}>
							<Arrows arrowsNext handleClick={this.goToTheNextCard} arrowsDisabled={startCard >= cards.length - 1}/>
						</div>
					</> : null}
				</div>
				{
					cards.length ?
					<MoreInfoDeal
						cardData={cards[startCard]}
						toggleModal={this.closeMoreInfoModal}
						modalIsOpen={moreInfoIsOpen}
						onSubmit={fetchEditCard}
						companies={companies}
						contacts={contacts}
					/> : null
				}
				<NewDeal
					toggleModal={this.toggleNewDealModal}
					modalIsOpen={newDealIsOpen}
					onSubmit={fetchAddCard}
					companies={companies}
					contacts={contacts}
				/>
			</div>
		);
	}
}

BoxDeal.propTypes = {
	style: PropTypes.object,
	cards: PropTypes.array,
	boxData: PropTypes.object,
};

export default BoxDeal;
