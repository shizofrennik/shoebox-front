import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import Modal from 'react-responsive-modal';
import HeaderForm from '../../../../components/Form/HeaderForm/HeaderForm';
import styles from './MoreInfoCompany.module.scss';
import InfoRowMutable from '../../../../components/InfoRowMutable/InfoRowMutable';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import DropdownCheckbox from '../../../../components/Form/DropdownCheckbox/DropdownCheckbox';
import Line from '../../../../components/Line/Line';
import Button from '../../../../components/Button/Button';
import ButtonIcon from '../../../../components/Button/ButtonIcon';

class MoreInfoCompany extends Component {
	render() {
		const { toggleModal, modalIsOpen, cardData, onSubmit, deals, contacts } = this.props;
		return (
			<Modal
				center
				onClose={toggleModal}
				open={modalIsOpen}
				showCloseIcon={false}
				classNames={ {modal: styles.contMoreInfo, overlay: styles.overlay} }
			>
				<Form
					onSubmit={(values) => {
						onSubmit(values);
						toggleModal();
					}}
					initialValues={cardData}
					validate={values => {
						const errors = {};
						if (!values.name) {
							errors.name = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, submitting, pristine}) => (
						<form onSubmit={handleSubmit}>
							<HeaderForm text={cardData.name} handleClick={toggleModal}/>
							<Field labelText="Company name" name="name" placeholder="For example, Waves Music" component={InfoRowMutable}/>
							<div className={[`${styles.dFlex} ${styles.row} ${styles.alignBottom}`]}>
								<Field labelText="Phone" name="phones" placeholder="4 654 455 76 98" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginLeft: '32px', marginBottom: '7px' }} btnSent type="sent" />
								<ButtonIcon style={{ marginLeft: '5px', marginBottom: '7px' }} btnSent type="call" />
							</div>
							<div className={[`${styles.dFlex} ${styles.row} ${styles.alignBottom}`]}>
								<Field labelText="E-mail" name="email" placeholder="wavesmusic@inbox.com" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginLeft: '5px', marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<Field labelText="Address 1" name="address_1" placeholder="For example, 580 Dark Hollow Road Oriental" component={InfoRowMutable}/>
							<Field labelText="Address 2" name="address_2" placeholder="For example, 580 Dark Hollow Road Oriental" component={InfoRowMutable}/>
							<Field labelText="State" name="state" placeholder="For example, New York" component={InfoRowMutable}/>
							<Field labelText="City" name="city" placeholder="For example, Buffalo New York" component={InfoRowMutable}/>
							<Field labelText="Zip" name="zip_code" placeholder="For example, 11716" component={InfoRowMutable}/>
							<Field labelText="Website" name="website" placeholder="For example, oqdxwtc.com" component={InfoRowMutable}/>
							<div className={styles.row}>
								<Field
									labelText="Source"
									name="source"
									placeholder="Trade show"
									component={Dropdown}
									options={optionsSource}
								/>
							</div>
							<div className={styles.row} style={{ marginTop: '25px' }}>
								<Field
									checkboxText="Added to campaign"
									placeholder="Choose Campaign Name"
									name="add_company_to_campaign"
									component={DropdownCheckbox}
									options={optionCampaign}
								/>
							</div>
							<div className={styles.row} style={{ marginTop: '25px' }}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated contacts"
									placeholder="Contact Name"
									name="contacts"
									component={Dropdown}
									options={contacts}
								/>
							</div>
							<div className={styles.row} style={{ marginTop: '25px' }}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated deal(s)"
									placeholder="Deals"
									name="deals"
									component={Dropdown}
									options={deals}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsDeals.find(o => o.value === item))}
								/>
							</div>
							<div className={styles.row} style={{ marginTop: '25px' }}>
								<Field
									labelText="Summary"
									placeholder="For example"
									name="summary"
									component={Textarea}
								/>
							</div>
							<div className={styles.row} style={{ marginTop: '25px' }}>
								<Field
									labelText="Add to box"
									name="box_id"
									component={Dropdown}
									options={optionsBox}
								/>
							</div>
							<Line style={{ marginTop: '29px' }} />
							<Line />
							<div style={{ marginTop: '20px' }} className={[`${styles.dFlex} ${styles.row} ${styles.alignCenter}`]}>
								<Field
									style={{width: '50%', marginTop: '5px'}}
									labelText=""
									placeholder="Choose fields template"
									name="template"
									component={Dropdown}
									options={optionsTemplate}
									// parse={val => val && val.value}
									// format={val => val && optionsTemplate.find(o => o.value === val.value)}
								/>
								<Button style={{ marginLeft: '18px' }} text="+ Add" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Current CRM"
									placeholder="Jefferson CRM"
									name="current_crm"
									component={Dropdown}
									options={optionsCurrentCRM}
								/>
							</div>
							<div className={styles.row}>
								<Field labelText="Size" name="size" placeholder="For example" component={InfoRowMutable}/>
							</div>
							<div className={styles.row}>
								<Field labelText="How long they've had current system" name="current_crm_used" placeholder="For example" component={InfoRowMutable}/>
							</div>
							<Button disabled={submitting || pristine} type="submit" style={{ marginTop: '30px' }} text="Save" full />
							<ButtonIcon style={{ marginTop: '25px' }} btnDrop type="drop" text="Drop back" />
						</form>
					)}
				>
				</Form>
			</Modal>
		);
	}
}



MoreInfoCompany.propTypes = {
	toggleModal: PropTypes.func,
	modalIsOpen: PropTypes.bool,
	cardData: PropTypes.object,
	onSubmit: PropTypes.func,
	deals: PropTypes.array,
	contacts: PropTypes.array,
};

const optionsTemplate = [
	{ value: 'Template-1', label: 'Template-1' },
	{ value: 'Template-2', label: 'Template-2' },
	{ value: 'Template-3', label: 'Template-3' },
	{ value: 'Template-4', label: 'Template-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsBox = [
	{ value: 1, label: 'Box-1' },
	{ value: 2, label: 'Box-2' },
	{ value: 3, label: 'Box-3' },
	{ value: 4, label: 'Box-4' },
];

const optionCampaign = [
	{ value: 'Campaign-1', label: 'Campaign-1' },
	{ value: 'Campaign-2', label: 'Campaign-2' },
	{ value: 'Campaign-3', label: 'Campaign-3' },
	{ value: 'Campaign-4', label: 'Campaign-4' },
];

export default MoreInfoCompany;
