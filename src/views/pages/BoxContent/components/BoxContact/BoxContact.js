import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from '../Head';
import NewContact from '../NewContact';
import MoreInfoContact from '../MoreInfoContact';
import Arrows from '../../../../components/Button/Arrows';
import Button from '../../../../components/Button/Button';
import Card from '../../../../components/Card/Card';
import Search from '../../../../components/Form/Search/Search';
import InfoRow from '../../../../components/InfoRow/InfoRow';
import ButtonIcon from '../../../../components/Button/ButtonIcon';
import DropdownCheckboxSingle from '../../../../components/Form/DropdownCheckbox/DropdownCheckboxSingle';
import DropdownSingle from '../../../../components/Form/Dropdown/DropdownSingle';
import styles from './BoxContact.module.scss';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import CheckboxSingle from '../../../../components/Form/Checkbox/CheckboxSingle';

class BoxContact extends Component {
	state = {
		moreInfoIsOpen: false,
		newContactIsOpen: false,
		startCard: 0,
	};

	goToTheNextCard = () => {
		this.setState({startCard: this.state.startCard + 1});
	}

	goToThePreviousCard = () => {
		this.setState({startCard: this.state.startCard - 1});
	}

	toggleMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: !this.state.moreInfoIsOpen})
	}

	closeMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: false})
	}

	toggleNewContactModal = () => {
		this.setState({newContactIsOpen: !this.state.newContactIsOpen})
	}

	render() {
		const { cards = [], boxData, fetchAddCard, fetchEditCard, goToPage, deals, companies } = this.props;
		const { moreInfoIsOpen, newContactIsOpen, startCard } = this.state;
		return (
			<div className={styles.containerContent}>
				<Head
					boxName={boxData.box_name}
					cardCount={cards.length}
					boxType="contact"
					style={{paddingTop: '13px'}}
					editBox={() => goToPage('/editboxcontact', { id: boxData.id })}
				/>
				<Search handleInput={() => {}} style={{marginLeft: '0px', marginTop: '20px', marginBottom: '15px'}}/>
				<TitleSection text="Contact" />
				<div className={styles.flexCont}>
					<Button text="+ Add new contact" handleClick={this.toggleNewContactModal}/>
					<Search handleInput={() => {}} />
					<div
						style={{
							fontFamily: 'Montserrat',
							fontStyle: 'normal',
							fontWeight: 'normal',
							fontSize: '14px',
							lineHeight: '17px',
							color: '#363F55',
						}}
					>
						All { cards.length } cards
					</div>
				</div>
				<div className={styles.cardContainer}>
					{cards.length ?
						<Card cardName={`${cards[startCard].first_name} ${cards[startCard].last_name}`} first>
						<div className={styles.flexCont}>
								<div className={styles.column}>
									<InfoRow headText="First Name" valueText={cards[startCard].first_name ||''} />
									<InfoRow headText="Last Name" valueText={cards[startCard].last_name ||''} />
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow headText="Mobile phone" valueText={cards[startCard].phone_mobile || ''} />
										<div style={{ display: 'flex', alignItems: 'flex-end' }}>
											<ButtonIcon style={{ marginLeft: '68px', marginBottom: '-5px' }} btnSent type="call" />
											<ButtonIcon style={{ marginLeft: '5px', marginBottom: '-5px' }} btnSent type="sent" />
										</div>
									</div>
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow headText="Work phone" valueText={cards[startCard].phone_work || ''} />
										<div style={{ display: 'flex', alignItems: 'flex-end' }}>
											<ButtonIcon style={{ marginLeft: '68px', marginBottom: '-5px' }} btnSent type="call" />
											<ButtonIcon style={{ marginLeft: '5px', marginBottom: '-5px' }} btnSent type="sent" />
										</div>
									</div>
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow headText="Work email" valueText={cards[startCard].work_email || ''} />
										<ButtonIcon style={{ marginBottom: '-5px' }} btnSent type="sent" />
									</div>
								</div>
								<div className={styles.column}>
									<InfoRow headText="Job title" valueText={cards[startCard].job_title || ''} />
									<div style={{marginTop: '27px'}}>
										<CheckboxSingle text="Viewed a demonstration" disabled/>
									</div>
									<div style={{marginTop: '20px'}}>
										<DropdownSingle
											labelText="Source"
											placeholder="No Source"
											value={cards[startCard].source}
											isDisabled
										/>
									</div>
									<div style={{marginTop: '20px', height: '25%'}}>
									<DropdownSingle
											isDisabled
											value={cards[startCard].companies}
											placeholder="Company"
											labelText="Associated company(ies)"
											linksIsActive
											isMulti
										/>
									</div>
									<div style={{marginTop: '20px'}}>
										<DropdownCheckboxSingle
											checkboxText="Added to campaign"
											placeholder="Choose Campaign Name"
											value={cards[startCard].campaigns}
											linksIsActive
											isMulti
										/>
									</div>
								</div>
								<div className={styles.column}>
									<div style={{ marginTop: '5px', height: '30%' }}>
										<DropdownSingle
											isDisabled
											value={cards[startCard].deals}
											placeholder="Deal"
											labelText="Associated deal(s)"
											linksIsActive
											isMulti
										/>
									</div>
									<div>
										<InfoRow
											headText="Summary"
											valueText={cards[startCard].summary}
										/>
									</div>
									<div style={{ marginTop: '25px' }}>
										<DropdownSingle
											labelText="Add to box"
											value={cards[0].box}
											isDisabled
										/>
									</div>
									<div
										className={`${styles.flexCont} ${styles.justifySpaceBetween}`}
										style={{ position: 'relative', width: '100%', top: '100px' }}
									>
										<Button
											style={{ width: 'calc(50% - 5px)' }}
											transparentBorder
											text="More Info"
											handleClick={this.toggleMoreInfoModal}
										/>
										<ButtonIcon style={{ width: 'calc(50% - 5px)' }} btnDrop type="drop" text="Drop back" />
									</div>
								</div>
							</div>
						</Card> : <Card empty/>}
						{ cards.length > 1 && (startCard + 1) < cards.length ?
							<Card cardName={`${cards[startCard + 1].first_name} ${cards[startCard + 1].last_name}`} second /> : null }
						{ cards.length > 1 && (startCard + 2) < cards.length ?
							<Card cardName={`${cards[startCard + 2].first_name} ${cards[startCard + 2].last_name}`} third /> : null }
						{ cards.length > 1 && (startCard + 3) < cards.length ?
							<Card cardName={`${cards[startCard + 3].first_name} ${cards[startCard + 3].last_name}`} fourth /> : null }
						{ cards.length > 1 && (startCard + 4) < cards.length ?
							<Card cardName={`${cards[startCard + 4].first_name} ${cards[startCard + 4].last_name}`} fifth /> : null }
						{ cards.length > 1 && (startCard + 5) < cards.length ?
							<Card cardName={`${cards[startCard + 5].first_name} ${cards[startCard + 5].last_name}`} sixth /> : null }
						{ cards.length > 1 && (startCard + 6) < cards.length ?
							<Card cardName={`${cards[startCard + 6].first_name} ${cards[startCard + 6].last_name}`} seventh /> : null }
						{ cards.length > 1 && (startCard + 7) < cards.length ?
							<Card cardName={`${cards[startCard + 7].first_name} ${cards[startCard + 7].last_name}`} eighth /> : null }
						{ cards.length > 1 ?
					<>
						<div className={[`${styles.contBut} ${styles.contBut_first}`]}>
							<Arrows handleClick={this.goToThePreviousCard} arrowsDisabled={startCard === 0}/>
						</div>
						<div className={[`${styles.contBut} ${styles.contBut_second}`]}>
							<Arrows arrowsNext handleClick={this.goToTheNextCard} arrowsDisabled={startCard >= cards.length - 1}/>
						</div>
					</> : null}
				</div>
				{
					cards.length ?
					<MoreInfoContact
						cardData={cards[startCard]}
						toggleModal={this.closeMoreInfoModal}
						modalIsOpen={moreInfoIsOpen}
						onSubmit={fetchEditCard}
						deals={deals}
						companies={companies}
					/> : null
				}
				<NewContact
					toggleModal={this.toggleNewContactModal}
					modalIsOpen={newContactIsOpen}
					onSubmit={fetchAddCard}
					deals={deals}
					companies={companies}
				/>
			</div>
		);
	}
}

BoxContact.propTypes = {
	cards: PropTypes.array,
	boxData: PropTypes.object,
	deals: PropTypes.array,
	companies: PropTypes.array,
};

export default BoxContact;
