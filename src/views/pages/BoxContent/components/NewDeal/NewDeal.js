import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import DropdownCheckbox from '../../../../components/Form/DropdownCheckbox/DropdownCheckbox';
import DatePick from '../../../../components/Form/DatePick/DatePick';
import Button from '../../../../components/Button/Button';
import ButtonIcon from '../../../../components/Button/ButtonIcon';
import HeaderForm from '../../../../components/Form/HeaderForm/HeaderForm';
import Input from '../../../../components/Form/Input/Input';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import RangeCheckbox from '../../../../components/Form/Range/RangeCheckbox';
import Line from '../../../../components/Line/Line';
import styles from './NewDeal.module.scss';

class NewDeal extends Component {
	// onSubmit = values => {
	// 	return new Promise(resolve => {
	// 		setTimeout(() => {
	// 			console.log(JSON.stringify(values, 0, 2));
	// 			resolve({
	// 				name: ['require'],
	// 			});
	// 		}, 200);
	// 	});
	// };
	render() {
		const { modalIsOpen, toggleModal, onSubmit, companies, contacts  } = this.props;
		return (
			<Modal
				onClose={toggleModal}
				center
				open={modalIsOpen}
				showCloseIcon={false}
				classNames={{ modal: styles.formNewDeal, overlay: styles.overlay }}
			>
				<Form
					onSubmit={(values) => {
						onSubmit(values);
						toggleModal();
					}}
					validate={values => {
						const errors = {};
						if (!values.title) {
							errors.title = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, submitting}) => (
						<form onSubmit={handleSubmit} >
							<HeaderForm text="Add New Deal" handleClick={toggleModal}/>
							<div className={styles.row}>
								<Field labelText="Deal" name="title" placeholder="For example, Waves Music" component={Input}/>
							</div>
							<div className={[`${styles.row} ${styles.rowFlex}`]}>
								<Field labelText="Predicted close date" name="predicted_close_date" component={DatePick} icon/>
								<Field labelText="From" name="from" component={DatePick} icon/>
							</div>
							<div className={styles.row}>
								<Field labelText="Deal amount" name="deal_amount" placeholder="For example, $100,000.00" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field labelText="Probability" name="probability" placeholder="For example, 10%" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field labelText="Adjusted forecast amount" name="adjusted_forecast_amount" placeholder="For example, 10%" component={Input}/>
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} ${styles.row}`]}>
								<Field labelText="Mobile phone" name="mobile_phone" placeholder="+7 999 999 99 99" component={Input}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px' }} btnSent type="call" />
								<ButtonIcon btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} ${styles.row}`]}>
								<Field labelText="Work email" name="work_email" placeholder="For example, ToddSteele@inbox.com" component={Input}/>
								<ButtonIcon style={{ marginLeft: '5px' }} btnSent type="sent" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Stage"
									name="stage"
									placeholder="Trade show"
									component={Dropdown}
									options={optionsStage}
									// parse={val => val && val.value}
									// format={val => val && optionsStage.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Source"
									name="stage"
									placeholder="Trade show"
									component={Dropdown}
									options={optionsSource}
									// parse={val => val && val.value}
									// format={val => val && optionsSource.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated contact(s)"
									placeholder="Contacts"
									name="contacts"
									component={Dropdown}
									options={contacts}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsContacts.find(o => o.value === item))}
								/>
							</div>
							<Field
								checkboxText="Added to campaign"
								placeholder="Choose Campaign Name"
								name="add_company_to_campaign"
								component={DropdownCheckbox}
								options={optionCampaign}
								parse={val => val && val.value}
								format={val => val && optionCampaign.find(o => o.value === val.value)}
							/>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated company"
									placeholder="Company"
									name="company"
									component={Dropdown}
									options={companies}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsCompany.find(o => o.value === item))}
								/>
							</div>
							<div className={styles.row}>
								<Field name="summary" labelText="Summary" placeholder="For example, Textarea" component={Textarea}/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Add to Box"
									name="box_id"
									placeholder="Hot prospects"
									component={Dropdown}
									options={optionsBoxes}
									// parse={val => val && val.value}
									// format={val => val && optionsBoxes.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field text="Note added" name="percent_goal" defaultValue={30} component={RangeCheckbox}/>
							</div>
							<Field type="checkbox" text="Call" name="goal" value="call" component={Checkbox} />
							<Field type="checkbox" text="Quote approved" name="goal" value="quote_approved" component={Checkbox} />
							<Field type="checkbox" text="Text sent" name="goal" value="text_sent" component={Checkbox} />
							<Line style={{ marginTop: '30px' }} />
							<Line />
							<div style={{ marginTop: '20px' }} className={[`${styles.dFlex} ${styles.row} ${styles.alignCenter}`]}>
								<Field
									style={{width: '50%', marginTop: '5px'}}
									labelText=""
									placeholder="Choose fields template"
									name="template"
									component={Dropdown}
									options={optionsTemplate}
									// parse={val => val && val.value}
									// format={val => val && optionsTemplate.find(o => o.value === val.value)}
								/>
								<Button style={{ marginLeft: '18px', height: '36px' }} text="+ Add" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Current CRM"
									name="current_crm"
									placeholder="Trade Jefferson CRM"
									component={Dropdown}
									options={optionsCurrentCRM}
									// parse={val => val && val.value}
									// format={val => val && optionsCurrentCRM.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field labelText="Size" name="size" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field labelText="How long they've had current system" name="current_crm_used" component={Input}/>
							</div>
							<Button disabled={submitting} type="submit" style={{ marginTop: '30px' }} text="Save" full />
						</form>
					)}
				>
				</Form>
			</Modal>
		);
	}
}

const optionsTemplate = [
	{ value: 'Template-1', label: 'Template-1' },
	{ value: 'Template-2', label: 'Template-2' },
	{ value: 'Template-3', label: 'Template-3' },
	{ value: 'Template-4', label: 'Template-4' },
];

const optionsBoxes = [
	{ value: 1, label: 'Box-1' },
	{ value: 2, label: 'Box-2' },
	{ value: 3, label: 'Box-3' },
	{ value: 4, label: 'Box-4' },
];

const optionsStage = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionCampaign = [
	{ value: 'Campaign-1', label: 'Campaign-1' },
	{ value: 'Campaign-2', label: 'Campaign-2' },
	{ value: 'Campaign-3', label: 'Campaign-3' },
	{ value: 'Campaign-4', label: 'Campaign-4' },
];

NewDeal.propTypes = {
	toggleModal: PropTypes.func,
	modalIsOpen: PropTypes.bool,
	onSubmit: PropTypes.func,
	companies: PropTypes.array,
	contacts: PropTypes.array,
};

export default NewDeal;
