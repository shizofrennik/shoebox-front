import React from 'react';
import PropTypes from 'prop-types';
import ButtonEdit from '../../../../components/Button/ButtonEdit';
import ButtonDelete from '../../../../components/Button/ButtonDelete';
import styles from './Head.module.scss';

function Head({boxName, cardCount, boxType, style, editBox, deleteBox}) {
	return (
		<div style={style} className={styles.cont}>
			<p className={[`${styles.flex} ${styles.boxInfo}`]}>
        All
				{
					boxType === 'company' ? ` companies ${cardCount}` :
						boxType === 'deal' ? ` deals ${cardCount}`:
							boxType === 'contact' ? ` contacts ${cardCount}`: null
				}
			</p>
			<div className={styles.flex}>
				<h1 className={styles.boxName}>{boxName.toUpperCase()}</h1>
				<ButtonEdit style={{marginTop: '7px', marginLeft: '23px'}} handleClick={editBox} />
				<ButtonDelete style={{marginTop: '7px', marginLeft: '10px'}} handleClick={deleteBox}/>
			</div>
			<p className={styles.boxInfo}>
				{
					boxType === 'company' ? `Companies ${cardCount}` :
						boxType === 'deal' ? `Deals ${cardCount}`:
							boxType === 'contact' ? `Contacts ${cardCount}`: null
				}
			</p>
		</div>
	);
}

Head.propTypes = {
	boxName: PropTypes.string,
	cardCount: PropTypes.number,
	boxType: PropTypes.string,
	style: PropTypes.object,
	editBox: PropTypes.func,
	deleteBox: PropTypes.func,
};

export default Head;
