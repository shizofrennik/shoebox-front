import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from '../Head';
import NewCompany from '../NewCompany';
import MoreInfoCompany from '../MoreInfoCompany';
import Arrows from '../../../../components/Button/Arrows';
import Button from '../../../../components/Button/Button';
import Card from '../../../../components/Card/Card';
import Search from '../../../../components/Form/Search/Search';
import InfoRow from '../../../../components/InfoRow/InfoRow';
import ButtonIcon from '../../../../components/Button/ButtonIcon';
import DropdownCheckboxSingle from '../../../../components/Form/DropdownCheckbox/DropdownCheckboxSingle';
import DropdownSingle from '../../../../components/Form/Dropdown/DropdownSingle';
import styles from './BoxCompany.module.scss';
import TitleSection from '../../../../components/TitleSection/TitleSection';

class BoxCompany extends Component {
	state = {
		moreInfoIsOpen: false,
		newCompanyIsOpen: false,
		startCard: 0,
	};

	goToTheNextPage = () => {
		this.setState({startCard: this.state.startCard + 1});
	}

	goToThePreviousPage = () => {
		this.setState({startCard: this.state.startCard - 1});
	}

	toggleMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: !this.state.moreInfoIsOpen})
	}

	closeMoreInfoModal = () => {
		this.setState({moreInfoIsOpen: false})
	}

	toggleNewCompanyModal = () => {
		this.setState({newCompanyIsOpen: !this.state.newCompanyIsOpen})
	}

	render() {
		const { cards = [], boxData, fetchAddCard, fetchEditCard, fetchDeleteBox, goToPage, deals, contacts } = this.props;
		const { moreInfoIsOpen, newCompanyIsOpen, startCard } = this.state;
		return (
			<div className={styles.containerContent}>
				<Head
					boxName={boxData.box_name}
					cardCount={cards.length}
					boxType="company"
					style={{paddingTop: '13px'}}
					editBox={() => goToPage('/editboxcompany', { id: boxData.id })}
					deleteBox={() => fetchDeleteBox(boxData.id)}
				/>
				<Search handleInput={() => {}} style={{marginLeft: '0px', marginTop: '20px', marginBottom: '15px'}}/>
				<TitleSection text="Company" />
				<div className={styles.flexCont}>
					<Button text="+ Add new company" handleClick={this.toggleNewCompanyModal}/>
					<Search handleInput={() => {}} />
					<div
						style={{
							fontFamily: 'Montserrat',
							fontStyle: 'normal',
							fontWeight: 'normal',
							fontSize: '14px',
							lineHeight: '17px',
							color: '#363F55',
						}}
					>
						All { cards.length } cards
					</div>
				</div>
				<div className={styles.cardContainer}>
					{cards.length ?
						<Card cardName={cards[startCard].name} first>
						<div className={styles.flexCont}>
								<div className={styles.column}>
									<InfoRow short headText="Company" valueText={cards[startCard].name ||''} />
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow short headText="Phone" valueText={cards[startCard].phones || ''} />
										<div style={{ display: 'flex', alignItems: 'flex-end' }}>
											<ButtonIcon style={{ marginLeft: '68px', marginBottom: '-5px' }} btnSent type="call" />
											<ButtonIcon style={{ marginLeft: '5px', marginBottom: '-5px' }} btnSent type="sent" />
										</div>
									</div>
									<div className={[`${styles.flexCont} ${styles.alignBottom} ${styles.justifySpaceBetween}`]}>
										<InfoRow short headText="E-mail" valueText={cards[startCard].email || ''} />
										<ButtonIcon style={{ marginBottom: '-5px' }} btnSent type="sent" />
									</div>
									<InfoRow short headText="Address 1" valueText={cards[startCard].address_1 || ''} />
									<InfoRow short headText="Address 2" valueText={cards[startCard].address_2 || ''} />
								</div>
								<div className={styles.column}>
									<InfoRow short headText="State" valueText={cards[0].state || ''} />
									<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
										<InfoRow short style={{ width: '60%' }} headText="City" valueText={cards[startCard].city || ''} />
										<InfoRow short style={{ width: '40%' }} headText="Zip" valueText={cards[startCard].zip_code || ''} />
									</div>
									<InfoRow headText="Website" valueText={cards[startCard].website || ''} />
									<div style={{ marginTop: '15px', marginBottom: '15px' }}>
										<DropdownSingle
											// placeholder={cards[0].source || ''}
											labelText="Source"
											value={cards[startCard].source || ''}
											isDisabled
										/>
									</div>
									<DropdownCheckboxSingle
										placeholder="Choose Campaign Name"
										labelText=""
										checkboxText="Added to campaign"
										linksIsActive
									/>
								</div>
								<div className={styles.column}>
									<div style={{ marginTop: '10px', height: '20%' }}>
										<DropdownSingle
											isDisabled
											linksIsActive
											isMulti
											limit
											options={cards[startCard].contacts}
											value={cards[startCard].contacts}
											labelText="Associated contact(s)"
											placeholder="Contact Name"
										/>
									</div>
									<div style={{ marginTop: '25px', height: '20%' }}>
										<DropdownSingle
											isDisabled
											linksIsActive
											isMulti
											limit
											options={cards[startCard].deals}
											value={cards[startCard].deals}
											labelText="Associated deal(s)"
											placeholder="Deals" />
									</div>
									<div>
										<InfoRow
											short
											headText="Summary"
											valueText={cards[startCard].summary}
										/>
									</div>
									<div style={{ marginTop: '25px' }}>
										<DropdownSingle
											labelText="Add to box"
											value={cards[startCard].box}
											isDisabled
										/>
									</div>
									<div className={`${styles.flexCont} ${styles.justifySpaceBetween}`} style={{ marginTop: '38px' }}>
										<Button
											style={{ width: 'calc(50% - 5px)' }}
											transparentBorder
											text="More Info"
											handleClick={this.toggleMoreInfoModal}
										/>
										<ButtonIcon style={{ width: 'calc(50% - 5px)' }} btnDrop type="drop" text="Drop back" />
									</div>
								</div>
							</div>
						</Card> : <Card empty/>}
						{ cards.length > 1 && (startCard + 1) < cards.length ? <Card cardName={cards[startCard + 1].name} second /> : null }
						{ cards.length > 2 && (startCard + 2) < cards.length ? <Card cardName={cards[startCard + 2].name} third /> : null }
						{ cards.length > 3 && (startCard + 3) < cards.length ? <Card cardName={cards[startCard + 3].name} fourth /> : null }
						{ cards.length > 4 && (startCard + 4) < cards.length ? <Card cardName={cards[startCard + 4].name} fifth /> : null }
						{ cards.length > 5 && (startCard + 5) < cards.length ? <Card cardName={cards[startCard + 5].name} sixth /> : null }
						{ cards.length > 6 && (startCard + 6) < cards.length ? <Card cardName={cards[startCard + 6].name} seventh /> : null }
						{ cards.length > 7 && (startCard + 7) < cards.length ? <Card cardName={cards[startCard + 7].name} eighth /> : null }
						{ cards.length > 1 ?
					<>
					<div className={[`${styles.contBut} ${styles.contBut_first}`]}>
							<Arrows handleClick={this.goToThePreviousPage} arrowsDisabled={startCard === 0}/>
						</div>
						<div className={[`${styles.contBut} ${styles.contBut_second}`]}>
							<Arrows arrowsNext handleClick={this.goToTheNextPage} arrowsDisabled={startCard >=cards.length - 1}/>
						</div>
					</> : null}
				</div>
				{
					cards.length ?
					<MoreInfoCompany
						cardData={cards[startCard]}
						onSubmit={fetchEditCard}
						toggleModal={this.closeMoreInfoModal}
						modalIsOpen={moreInfoIsOpen}
						deals={deals}
						contacts={contacts}
					/> : null
				}
				<NewCompany
					toggleModal={this.toggleNewCompanyModal}
					modalIsOpen={newCompanyIsOpen}
					onSubmit={fetchAddCard}
					deals={deals}
					contacts={contacts}
				/>
			</div>
		);
	}
}

BoxCompany.propTypes = {
	cards: PropTypes.array,
	style: PropTypes.object,
	boxData: PropTypes.object,
	deals: PropTypes.array,
	contacts: PropTypes.array,
};

export default BoxCompany;
