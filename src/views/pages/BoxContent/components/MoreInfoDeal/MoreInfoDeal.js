import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import HeaderForm from '../../../../components/Form/HeaderForm/HeaderForm';
import styles from './MoreInfoDeal.module.scss';
import InfoRowMutable from '../../../../components/InfoRowMutable/InfoRowMutable';
import DatePick from '../../../../components/Form/DatePick/DatePick';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import Line from '../../../../components/Line/Line';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import RangeCheckbox from '../../../../components/Form/Range/RangeCheckbox';
import DropdownCheckbox from '../../../../components/Form/DropdownCheckbox/DropdownCheckbox';
import Button from '../../../../components/Button/Button';
import ButtonIcon from '../../../../components/Button/ButtonIcon';

class MoreInfoDeal extends Component {
	render() {
		const { toggleModal, modalIsOpen, cardData, onSubmit, companies, contacts } = this.props;
		return (
			<Modal
				center
				onClose={toggleModal}
				open={modalIsOpen}
				showCloseIcon={false}
				classNames={ {modal: styles.contMoreInfo, overlay: styles.overlay} }
			>
				<Form
					initialValues={cardData}
					onSubmit={(values) => {
						onSubmit(values);
						toggleModal();
					}}
					validate={values => {
						const errors = {};
						if (!values.title) {
							errors.title = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, submitting, pristine}) => (
						<form onSubmit={handleSubmit} >
							<HeaderForm text={cardData.title || ''} handleClick={toggleModal}/>
							<Field labelText="Deal" name="title" component={InfoRowMutable}/>
							<div className={[`${styles.row} ${styles.rowFlex}`]}>
								<Field labelText="Predicted close date" name="predicted_close_date" component={DatePick} icon/>
								<Field labelText="From" name="from" component={DatePick} icon/>
							</div>
							<Field labelText="Deal amount" name="deal_amount" placeholder="For example, $100,000.00" component={InfoRowMutable}/>
							<Field labelText="Probability" name="probability" placeholder="For example, 10%" component={InfoRowMutable}/>
							<Field labelText="Adjusted forecast amount" name="adjusted_forecast_amount" placeholder="For example, 10%" component={InfoRowMutable}/>
							<div className={[`${styles.dFlex} ${styles.alignBottom}`]}>
								<Field labelText="Mobile phone" name="mobile_phone" placeholder="+7 999 999 99 99" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px', marginBottom: '7px' }} btnSent type="call" />
								<ButtonIcon style={{ marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom}`]}>
								<Field labelText="Work email" name="work_email" placeholder="For example, ToddSteele@inbox.com" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginLeft: '5px', marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Stage"
									name="stage"
									placeholder="Choose Stage"
									component={Dropdown}
									options={optionsStage}
									// parse={val => val && val.value}
									// format={val => val && optionsStage.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Source"
									name="source"
									placeholder="Choose Source"
									component={Dropdown}
									options={optionsSource}
									// parse={val => val && val.value}
									// format={val => val && optionsSource.find(o => o.value === val.value)}

								/>
							</div>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated contact(s)"
									placeholder="Contacts"
									name="contacts"
									component={Dropdown}
									options={contacts}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsContacts.find(o => o.value === item))}
								/>
							</div>
							<Field
								checkboxText="Added to campaign"
								placeholder="Choose Campaign Name"
								name="add_company_to_campaign"
								component={DropdownCheckbox}
								options={optionCampaign}
								// parse={val => val && val.value}
								// format={val => val && optionCampaign.find(o => o.value === val.value)}
							/>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated company(ies)"
									placeholder="Company"
									name="companies"
									component={Dropdown}
									options={companies}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsCompany.find(o => o.value === item))}
								/>
							</div>
							<div className={styles.row}>
								<Field name="summary" labelText="Summary" placeholder="For example, Textarea" component={Textarea}/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Add to Box"
									name="box"
									placeholder="Hot prospects"
									component={Dropdown}
									options={optionsBoxes}
									// parse={val => val && val.value}
									// format={val => val && optionsBoxes.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field
									text="Note added"
									name="percent_goal"
									component={RangeCheckbox}
									valueCheckbox={cardData.percent_goal ? true : false}
								/>
							</div>
							<Field type="checkbox" text="Call" name="goal" value="call" component={Checkbox} />
							<Field type="checkbox" text="Quote approved" name="goal" value="quote_approved" component={Checkbox} />
							<Field type="checkbox" text="Text sent" name="goal" value="text_sent" component={Checkbox} />
							<Line style={{ marginTop: '30px' }} />
							<Line />
							<div style={{ marginTop: '20px' }} className={[`${styles.dFlex} ${styles.row} ${styles.alignCenter}`]}>
								<Field
									style={{width: '50%', marginTop: '5px'}}
									labelText=""
									placeholder="Choose fields template"
									name="template"
									component={Dropdown}
									options={optionsTemplate}
									// parse={val => val && val.value}
									// format={val => val && optionsTemplate.find(o => o.value === val.value)}
								/>
								<Button style={{ marginLeft: '18px', height: '36px' }} text="+ Add" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Current CRM"
									name="current_crm"
									placeholder="Trade Jefferson CRM"
									component={Dropdown}
									options={optionsCurrentCRM}
									// parse={val => val && val.value}
									// format={val => val && optionsCurrentCRM.find(o => o.value === val.value)}
								/>
							</div>
							<Field labelText="Size" name="size" component={InfoRowMutable}/>
							<Field labelText="How long they've had current system" name="current_crm_used" component={InfoRowMutable}/>
							<Button disabled={submitting || pristine} type="submit" style={{ marginTop: '30px' }} text="Save" full />
							<ButtonIcon style={{ marginTop: '25px' }} btnDrop type="drop" text="Drop back" />
						</form>
					)}
				>
				</Form>
			</Modal>
		);
	}
}

MoreInfoDeal.propTypes = {
	toggleModal: PropTypes.func,
	modalIsOpen: PropTypes.bool,
	cardData: PropTypes.object,
	onSubmit: PropTypes.func,
	companies: PropTypes.array,
	contacts: PropTypes.array,
};

const optionsStage = [
	{ value: 'Stage-1', label: 'Stage-1' },
	{ value: 'Stage-2', label: 'Stage-2' },
	{ value: 'Stage-3', label: 'Stage-3' },
	{ value: 'Stage-4', label: 'Stage-4' },
];

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsBoxes = [
	{ value: 1, label: 'Box-1' },
	{ value: 2, label: 'Box-2' },
	{ value: 3, label: 'Box-3' },
	{ value: 4, label: 'Box-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionsTemplate = [
	{ value: 'Template-1', label: 'Template-1' },
	{ value: 'Template-2', label: 'Template-2' },
	{ value: 'Template-3', label: 'Template-3' },
	{ value: 'Template-4', label: 'Template-4' },
];

const optionCampaign = [
	{ value: 'Campaign-1', label: 'Campaign-1' },
	{ value: 'Campaign-2', label: 'Campaign-2' },
	{ value: 'Campaign-3', label: 'Campaign-3' },
	{ value: 'Campaign-4', label: 'Campaign-4' },
];

export default MoreInfoDeal;
