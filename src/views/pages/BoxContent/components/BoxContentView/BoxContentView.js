import React, {Component} from 'react';
import PropTypes from 'prop-types';
import BoxDeal from '../BoxDeal';
import BoxCompany from '../BoxCompany';
import BoxContact from '../BoxContact';

class BoxContentView extends Component {
	render() {
		const  {cards, boxData, history, fetchAddCard, fetchEditCard, deals, companies, contacts, fetchDeleteBox} = this.props;
		return (
			<div>
				{ history.location.state.type === 'deal' ?
					<BoxDeal
						boxData={boxData}
						cards={cards}
						fetchAddCard={fetchAddCard}
						fetchEditCard={fetchEditCard}
						fetchDeleteBox={fetchDeleteBox}
						goToPage={this.props.history.push}
						companies={companies}
						contacts={contacts}
					/> :
					history.location.state.type === 'company' ?
						<BoxCompany
							boxData={boxData}
							cards={cards}
							fetchAddCard={fetchAddCard}
							fetchEditCard={fetchEditCard}
							fetchDeleteBox={fetchDeleteBox}
							goToPage={this.props.history.push}
							contacts={contacts}
							deals={deals}
						/> :
						history.location.state.type === 'contact' ?
							<BoxContact
								boxData={boxData}
								cards={cards}
								fetchAddCard={fetchAddCard}
								fetchEditCard={fetchEditCard}
								fetchDeleteBox={fetchDeleteBox}
								goToPage={this.props.history.push}
								deals={deals}
								companies={companies}
							/>
							: 0
				}
			</div>
		);
	}

}

BoxContentView.propTypes = {
	type: PropTypes.string,
	cards: PropTypes.array,
	history: PropTypes.object,
	boxData: PropTypes.object,
	fetchAddCard: PropTypes.func,
	fetchEditCard: PropTypes.func,
	fetchDeleteBox: PropTypes.func,
	deals: PropTypes.array,
	companies: PropTypes.array,
	contacts: PropTypes.array,
};

export default BoxContentView;
