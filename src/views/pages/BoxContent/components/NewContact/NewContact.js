import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import DropdownCheckbox from '../../../../components/Form/DropdownCheckbox/DropdownCheckbox';
import Button from '../../../../components/Button/Button';
import ButtonIcon from '../../../../components/Button/ButtonIcon';
import HeaderForm from '../../../../components/Form/HeaderForm/HeaderForm';
import Input from '../../../../components/Form/Input/Input';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import Line from '../../../../components/Line/Line';
import styles from './NewContact.module.scss';

class NewContact extends Component {
	// onSubmit = values => {
	// 	return new Promise(resolve => {
	// 		setTimeout(() => {
	// 			console.log(JSON.stringify(values, 0, 2));
	// 			resolve({
	// 				name: ['require'],
	// 			});
	// 		}, 200);
	// 	});
	// };
	render() {
		const { modalIsOpen, toggleModal, onSubmit, deals, companies } = this.props;
		return (
			<Modal
				onClose={toggleModal}
				center
				open={modalIsOpen}
				showCloseIcon={false}
				classNames={{ modal: styles.formNewDeal, overlay: styles.overlay }}
			>
				<Form
					onSubmit={(values) => {
						onSubmit(values);
						toggleModal();
					}}
					validate={values => {
						const errors = {};
						if (!values.first_name) {
							errors.first_name = 'Required';
						}
						if (!values.last_name) {
							errors.last_name = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, submitting}) => (
						<form onSubmit={handleSubmit} >
							<HeaderForm text="Add New Contact" handleClick={toggleModal}/>
							<div className={styles.row}>
								<Field labelText="First name" name="first_name" placeholder="For example, Peter" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field labelText="Last Name" name="last_name" placeholder="For example, Jackson" component={Input}/>
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} ${styles.row}`]}>
								<Field labelText="Mobile phone" name="phone_mobile" placeholder="456 564 4532" component={Input}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px' }} btnSent type="call" />
								<ButtonIcon btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} ${styles.row}`]}>
								<Field labelText="Work phone" name="phone_work" placeholder="456 564 4532" component={Input}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px' }} btnSent type="call" />
								<ButtonIcon btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} ${styles.row}`]}>
								<Field labelText="Work email" name="work_email" placeholder="peterjackson@gmail.com" component={Input}/>
								<ButtonIcon style={{ marginLeft: '5px' }} btnSent type="sent" />
							</div>
							<div className={styles.row}>
								<Field labelText="Job title" name="job_title" placeholder="For example, Manager" component={Input}/>
							</div>
							<Field type="checkbox" text="Viewed a demonstration" name="viewed_a_demonstration" value='true' component={Checkbox} />
							<div className={styles.row2}>
								<Field
									labelText="Source"
									name="stage"
									placeholder="Trade show"
									component={Dropdown}
									options={optionsSource}
								/>
							</div>
							<div className={styles.row2}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated company(ies)"
									placeholder="Company"
									name="company"
									component={Dropdown}
									options={companies}
								/>
							</div>
							<Field
								checkboxText="Added to campaign"
								placeholder="Choose Campaign Name"
								name="add_company_to_campaign"
								component={DropdownCheckbox}
								options={optionCampaign}
								// parse={val => val && val.value}
								// format={val => val && optionCampaign.find(o => o.value === val.value)}
							/>
							<Field
								isMulti
								linksIsActive
								labelText="Associated deal(s)"
								placeholder="Deal"
								name="deal"
								component={Dropdown}
								options={deals}
							/>
							<div className={styles.row}>
								<Field name="summary" labelText="Summary" placeholder="For example, Textarea" component={Textarea}/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Add to Box"
									name="box_id"
									placeholder="Hot prospects"
									component={Dropdown}
									options={optionsBoxes}
								/>
							</div>
							<Line/>
							<Line/>
							<div style={{ marginTop: '20px' }} className={[`${styles.dFlex} ${styles.row} ${styles.alignCenter}`]}>
								<Field
									style={{width: '50%', marginTop: '5px'}}
									labelText=""
									placeholder="Choose fields template"
									name="template"
									component={Dropdown}
									options={optionsTemplate}
								/>
								<Button style={{ marginLeft: '18px', height: '36px' }} text="+ Add" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Current CRM"
									name="current_crm"
									placeholder="Trade Jefferson CRM"
									component={Dropdown}
									options={optionsCurrentCRM}
									// parse={val => val && val.value}
									// format={val => val && optionsCurrentCRM.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field labelText="Size" name="size" placeholder="For example" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field labelText="How long they've had current system" name="current_crm_used" placeholder="For example" component={Input}/>
							</div>
							<Button disabled={submitting} type="submit" style={{ marginTop: '30px' }} text="Save" full />
						</form>
					)}
				>
				</Form>
			</Modal>
		);
	}
}

const optionsTemplate = [
	{ value: 'Template-1', label: 'Template-1' },
	{ value: 'Template-2', label: 'Template-2' },
	{ value: 'Template-3', label: 'Template-3' },
	{ value: 'Template-4', label: 'Template-4' },
];

const optionsBoxes = [
	{ value: 1, label: 'Box-1' },
	{ value: 2, label: 'Box-2' },
	{ value: 3, label: 'Box-3' },
	{ value: 4, label: 'Box-4' },
];

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionCampaign = [
	{ value: 'Campaign-1', label: 'Campaign-1' },
	{ value: 'Campaign-2', label: 'Campaign-2' },
	{ value: 'Campaign-3', label: 'Campaign-3' },
	{ value: 'Campaign-4', label: 'Campaign-4' },
];

NewContact.propTypes = {
	toggleModal: PropTypes.func,
	modalIsOpen: PropTypes.bool,
	onSubmit: PropTypes.func,
	deals: PropTypes.array,
	companies: PropTypes.array,
};

export default NewContact;
