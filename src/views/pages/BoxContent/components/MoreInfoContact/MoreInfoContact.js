import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import HeaderForm from '../../../../components/Form/HeaderForm/HeaderForm';
import styles from './MoreInfoContact.module.scss';
import InfoRowMutable from '../../../../components/InfoRowMutable/InfoRowMutable';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import Line from '../../../../components/Line/Line';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import DropdownCheckbox from '../../../../components/Form/DropdownCheckbox/DropdownCheckbox';
import Button from '../../../../components/Button/Button';
import ButtonIcon from '../../../../components/Button/ButtonIcon';

class MoreInfoContact extends Component {
	render() {
		const { toggleModal, modalIsOpen, cardData, onSubmit, deals, companies } = this.props;
		return (
			<Modal
				center
				onClose={toggleModal}
				open={modalIsOpen}
				showCloseIcon={false}
				classNames={ {modal: styles.contMoreInfo, overlay: styles.overlay} }
			>
				<Form
					initialValues={cardData}
					onSubmit={(values) => {
						onSubmit(values);
						toggleModal();
					}}
					validate={values => {
						const errors = {};
						if (!values.title) {
							errors.title = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, submitting, pristine}) => (
						<form onSubmit={handleSubmit} >
							<HeaderForm text={`${cardData.first_name} ${cardData.last_name}`} handleClick={toggleModal}/>
							<Field labelText="First name" name="first_name" placeholder="For example, Peter" component={InfoRowMutable}/>
							<Field labelText="Last Name" name="last_name" placeholder="For example, Jackson" component={InfoRowMutable}/>
							<div className={[`${styles.dFlex} ${styles.alignBottom} `]}>
								<Field labelText="Mobile phone" name="phone_mobile" placeholder="456 564 4532" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px', marginBottom: '7px' }} btnSent type="call" />
								<ButtonIcon style={{ marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} `]}>
								<Field labelText="Work phone" name="phone_work" placeholder="456 564 4532" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginRight: '5px', marginLeft: '5px', marginBottom: '7px' }} btnSent type="call" />
								<ButtonIcon style={{ marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<div className={[`${styles.dFlex} ${styles.alignBottom} `]}>
								<Field labelText="Work email" name="work_email" placeholder="peterjackson@gmail.com" component={InfoRowMutable}/>
								<ButtonIcon style={{ marginLeft: '5px', marginBottom: '7px' }} btnSent type="sent" />
							</div>
							<Field labelText="Job title" name="job_title" placeholder="For example, Manager" component={InfoRowMutable}/>
							<Field type="checkbox" text="Viewed a demonstration" name="viewed_a_demonstration" value='true' component={Checkbox} />
							<div className={styles.row}>
								<Field
									labelText="Source"
									name="stage"
									placeholder="Trade show"
									component={Dropdown}
									options={optionsSource}
								/>
							</div>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated company(ies)"
									placeholder="Company"
									name="companies"
									component={Dropdown}
									options={companies}
								/>
							</div>
							<div className={styles.row}>
								<Field
									checkboxText="Added to campaign"
									placeholder="Choose Campaign Name"
									name="add_company_to_campaign"
									component={DropdownCheckbox}
									options={optionCampaign}
								// parse={val => val && val.value}
								// format={val => val && optionCampaign.find(o => o.value === val.value)}
								/>
							</div>
							<div className={styles.row}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated deal(s)"
									placeholder="Deal"
									name="deals"
									component={Dropdown}
									options={deals}
								/>
							</div>
							<div className={styles.row}>
								<Field name="summary" labelText="Summary" placeholder="For example, Textarea" component={Textarea}/>
							</div>
							<div className={styles.row}>
								<Field
									labelText="Add to Box"
									name="box_id"
									placeholder="Hot prospects"
									component={Dropdown}
									options={optionsBoxes}
								/>
							</div>
							<Line/>
							<Line/>
							<div style={{ marginTop: '20px' }} className={[`${styles.dFlex} ${styles.row} ${styles.alignCenter}`]}>
								<Field
									style={{width: '50%', marginTop: '5px'}}
									labelText=""
									placeholder="Choose fields template"
									name="template"
									component={Dropdown}
									options={optionsTemplate}
								/>
								<Button style={{ marginLeft: '18px', height: '36px' }} text="+ Add" />
							</div>
							<div className={styles.row}>
								<Field
									labelText="Current CRM"
									name="current_crm"
									placeholder="Trade Jefferson CRM"
									component={Dropdown}
									options={optionsCurrentCRM}
									// parse={val => val && val.value}
									// format={val => val && optionsCurrentCRM.find(o => o.value === val.value)}
								/>
							</div>
							<Field labelText="Size" name="size" placeholder="For example" component={InfoRowMutable}/>
							<Field labelText="How long they've had current system" name="current_crm_used" placeholder="For example" component={InfoRowMutable}/>
							<Button disabled={submitting || pristine} type="submit" style={{ marginTop: '30px' }} text="Save" full />
							<ButtonIcon style={{ marginTop: '25px' }} btnDrop type="drop" text="Drop back" />
						</form>
					)}
				>
				</Form>
			</Modal>
		);
	}
}

MoreInfoContact.propTypes = {
	toggleModal: PropTypes.func,
	modalIsOpen: PropTypes.bool,
	cardData: PropTypes.object,
	onSubmit: PropTypes.func,
	deals: PropTypes.array,
	companies: PropTypes.array,
};

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

const optionsBoxes = [
	{ value: 1, label: 'Box-1' },
	{ value: 2, label: 'Box-2' },
	{ value: 3, label: 'Box-3' },
	{ value: 4, label: 'Box-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionsTemplate = [
	{ value: 'Template-1', label: 'Template-1' },
	{ value: 'Template-2', label: 'Template-2' },
	{ value: 'Template-3', label: 'Template-3' },
	{ value: 'Template-4', label: 'Template-4' },
];

const optionCampaign = [
	{ value: 'Campaign-1', label: 'Campaign-1' },
	{ value: 'Campaign-2', label: 'Campaign-2' },
	{ value: 'Campaign-3', label: 'Campaign-3' },
	{ value: 'Campaign-4', label: 'Campaign-4' },
];

export default MoreInfoContact;
