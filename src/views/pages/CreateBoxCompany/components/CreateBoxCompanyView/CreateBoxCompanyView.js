import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Form, Field } from 'react-final-form';
import Button from '../../../../components/Button/Button';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import Input from '../../../../components/Form/Input/Input';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import DropdownSingle from '../../../../components/Form/Dropdown/DropdownSingle';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import CheckboxSingle from '../../../../components/Form/Checkbox/CheckboxSingle';
import RadioBtn from '../../../../components/Form/RadioBtn/RadioBtn';
import Line from '../../../../components/Line/Line';
import styles from './CreateBoxCompanyView.module.scss';

// eslint-disable-next-line class-methods-use-this
class CreateBoxCompanyView extends Component {
	state = {
		updatesIsActive: false,
		boxType: { value: 'company', label: 'Company' },
	}

	onSubmit = values => {
		return new Promise(resolve => {
			setTimeout(() => {
				// console.log(JSON.stringify(values, 0, 2));
				resolve({
					box_name: ['require'],
				});
			}, 200);
		});
	};

	toggleUpdatesCheckboxes = () => {
		this.setState({updatesIsActive: !this.state.updatesIsActive})
	}

	changeBoxType = (value) => {
		this.setState({ boxType: value })
	}

	render() {
		const { updatesIsActive, boxType } = this.state;
		return (
			<>
				{ boxType.value === "contact" ? <Redirect to="/newboxcontact"/> :
					boxType.value === "deal"	? <Redirect to="/newboxdeal"/> : null
				}
				<Form
					onSubmit={this.onSubmit}
					validate={values => {
						const errors = {};
						if (!values.name) {
							errors.name = 'Required';
						}
						return errors;
					}}
					render={ ( {handleSubmit} ) => (
					<div className={styles.containerBoxForm}>
						<TitleSection text="Create Box for Company" />
							<form onSubmit={handleSubmit} className={styles.formNewBox}>
								<h3>Box</h3>
								<div className={styles.rowForm}>
									<Field labelText="Box Name" name="name" placeholder="For example, Hot Prospects" component={Input}/>
								</div>
								<div className={styles.rowForm}>
								{/* <Field labelText="Choose Type" placeholder="Company" name="type" component={Dropdown}/> */}
								<DropdownSingle
									labelText="Choose Type"
									placeholder="Company"
									name="type"
									handleChange={this.changeBoxType}
									value={boxType}
								/>
							</div>
								<div className={styles.rowForm}>
									<Field labelText="State" name="state" placeholder="For example, California" component={Input}/>
								</div>
								<div className={styles.rowForm}>
									<Field labelText="City" name="city" placeholder="For example, Los Angeles" component={Input}/>
								</div>
								<div className={styles.rowForm}>
									<Field labelText="Zip Code" name="zip_code" placeholder="For example, 21347" component={Input}/>
								</div>
								<div className={styles.rowForm}>
									<Field
										labelText="Source"
										name="source"
										placeholder="International Kitchen and Bath Trade Show 2019"
										component={Dropdown}
										options={optionsSource}
										parse={val => val && val.value}
										format={val => val && optionsSource.find(o => o.value === val.value)}
									/>
								</div>
								<div className={styles.rowForm}>
									<Field
										isMulti
										labelText="Associated deal(s)"
										name="deals"
										placeholder="Deal"
										component={Dropdown}
										options={optionsDeals}
										parse={val => val && val.map(item => item.value)}
										format={val => val && val.map(item => optionsDeals.find(o => o.value === item))}
									/>
								</div>
								<div className={styles.rowForm}>
									<Field
										labelText="Current CRM"
										name="current_crm"
										placeholder="Jefferson CRM"
										component={Dropdown}
										options={optionsCurrentCRM}
										parse={val => val && val.value}
										format={val => val && optionsCurrentCRM.find(o => o.value === val.value)}
									/>
								</div>
								<div className={styles.rowForm}>
									<Field labelText="Size" name="size" placeholder="" component={Input}/>
								</div>
								<div className={styles.rowForm}>
									<Field labelText="How long they've had current system" name="current_crm_used" placeholder="" component={Input}/>
								</div>
								<Line style={{ marginTop: '30px', marginBottom: '10px' }} />
								<div className={styles.rowCards}>
									<p>
										Cards <span>344</span>
									</p>
									<p className={styles.rowCards__all}>View all cards</p>
								</div>
								<h4>Assign the box</h4>
								<div className={styles.rowForm}>
									<Field labelText="To" placeholder="" name="assined_to" component={Input}/>
								</div>
								<div className={[`${styles.rowTextarea} ${styles.rowForm}`]}>
									<Field labelText="Description of assignment" name="description_of_assignment" placeholder="" component={Textarea}/>
								</div>
								<CheckboxSingle name="updates" text="Get updates:" handleChange={this.toggleUpdatesCheckboxes}/>
								<div className={[`${styles.rowRadio} ${styles.rowForm}`]}>
									<Field
										type="radio"
										name="updates"
										value="daily"
										text="Daily"
										disabled={updatesIsActive ? false : true}
										component={RadioBtn}
									/>
									<Field
										type="radio"
										name="updates"
										value="weekly"
										text="Weekly"
										disabled={updatesIsActive ? false : true}
										component={RadioBtn}/>
									<Field
										type="radio"
										name="updates"
										value="happens"
										text="As it happens"
										disabled={updatesIsActive ? false : true}
										component={RadioBtn}/>
								</div>
								<Button text="Send" full type="submit"/>
							</form>
					</div>
				) }/>
			</>
		);
	}
}

const optionsDeals = [
	{ value: 1, label: 'Deal-1' },
	{ value: 2, label: 'Deal-2' },
	{ value: 3, label: 'Deal-3' },
	{ value: 4, label: 'Deal-4' },
];

const optionsCurrentCRM = [
	{ value: 'CurrentCRM-1', label: 'CurrentCRM-1' },
	{ value: 'CurrentCRM-2', label: 'CurrentCRM-2' },
	{ value: 'CurrentCRM-3', label: 'CurrentCRM-3' },
	{ value: 'CurrentCRM-4', label: 'CurrentCRM-4' },
];

const optionsSource = [
	{ value: 'Source-1', label: 'Source-1' },
	{ value: 'Source-2', label: 'Source-2' },
	{ value: 'Source-3', label: 'Source-3' },
	{ value: 'Source-4', label: 'Source-4' },
];

export default CreateBoxCompanyView;
