import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import styles from './LogInView.module.scss';
import CreateAccount from '../CreateAccount';
import Input from '../../../../components/Form/Input/Input';
import Button from '../../../../components/Button/Button';

class LogInView extends Component {
	render() {
		const { user, isAuthenticated, fetchAuth, accountIsCreated, fetchCreateAccount } = this.props;
		return (
			<>
				{isAuthenticated ? <Redirect to="/"/> : null }
				<Form
					onSubmit={fetchAuth}
					validate={values => {
						const errors = {};
						if (!values.email) {
							errors.email = 'Required';
						}
						else if(!values.email.match( /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
							errors.email = 'Please enter a valid e-mail address';
						}
						if (!values.password) {
							errors.password = 'Required';
						}
						return errors;
					}}
					render={({handleSubmit, pristine, form}) => (
						<form onSubmit={handleSubmit} className={styles.form} >
							<h1 className={styles.head}>Log In</h1>
							<div className={styles.row}>
								<Field name="email" labelText="Email" placeholder="For example, goodemail@gmail.com" component={Input}/>
							</div>
							<div className={styles.row}>
								<Field type="password" name="password" labelText="Password" placeholder="For example, ********" component={Input}/>
							</div>
							<div className={styles.bigRow}>
								<Button
									full
									text="Log In"
									disabled={pristine}
									type="submit" />
							</div>
							<div className={styles.row}>
								<Button type="button" full transparentBorder text="Reset" handleClick={form.reset} />
							</div>
							{user.name && <p>User: {user.name}</p>}
						</form>
					) } />
					<CreateAccount accountIsCreated={accountIsCreated} fetchCreateAccount={fetchCreateAccount}/>
			</>
		);
	}
}

LogInView.propTypes = {
	user: PropTypes.object,
	fetchAuth: PropTypes.func,
	isAuthenticated: PropTypes.bool,
	accountIsCreated: PropTypes.string,
	fetchCreateAccount: PropTypes.func,
};

export default LogInView;
