import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import LogInView from '../components/LogInView';
import { authActions } from '../../../../state/ducks/auth';

const { fetchAuth, fetchCreateAccount } = authActions;

const mapStateToProps = (state) => {
	return ({
		user: state.auth.user,
		isAuthenticated: state.auth.isAuthenticated,
		accountIsCreated: state.auth.accountIsCreated,
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchAuth,
		fetchCreateAccount,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LogInView);
