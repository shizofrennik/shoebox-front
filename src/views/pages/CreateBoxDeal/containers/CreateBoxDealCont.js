import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import CreateBoxDealView from '../components/CreateBoxDealView';
// import { boxActions } from '../../../../state/ducks/box';
// import { boxSelectors } from '../../../../state/ducks/box';

// const { fetchSearchCards, fetchAddCard, fetchEditCard } = boxActions;
// const { getDataOfActiveBox } = boxSelectors;

const mapStateToProps = () => {
	return ({
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		// fetchAuth
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateBoxDealView);