import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BoxesView from '../components/BoxesView';
import { boxActions } from '../../../../state/ducks/box';

const { fetchGetBoxes, fetchDeleteBox } = boxActions;

const mapStateToProps = (state) => {
	return ({
		boxes: state.box.boxes,
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchGetBoxes,
		fetchDeleteBox,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(BoxesView);