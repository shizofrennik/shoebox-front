import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ButtonEdit from '../../../../components/Button/ButtonEdit';
import ButtonDelete from '../../../../components/Button/ButtonDelete';
import styles from './Box.module.scss';

class BoxItem extends Component {
	render() {
		const { countCards, title, subtitle, user, date, goToPage, id, type, fetchDeleteBox } = this.props;
		return (
			<div
				className={styles.boxItem}
				onClick={
					() => goToPage(`/boxes/${id}`, { id, type })
				}
			>
				<div className={styles.boxItem__line} />
				<div className={styles.boxItem__control}>
					<p>Total cards {countCards}</p>
					<div className={styles.itemHeaderControl}>
						{
							type === 'deal' ? <ButtonEdit handleClick={() => goToPage('/editboxdeal', { id })}/> :
								type === 'contact' ? <ButtonEdit handleClick={() => goToPage('/editboxcontact', { id })}/> :
									type === 'company' ? <ButtonEdit handleClick={() => goToPage('/editboxcompany', { id })}/> :	 null
						}
						<ButtonDelete
							handleClick={(e) => {
								fetchDeleteBox(id);
								e.stopPropagation();
							}}/>
					</div>
				</div>
				<h3
					className={styles.boxItem__title}>
					{title}
				</h3>
				<p className={styles.boxItem__category}>{subtitle}</p>
				<p className={styles.boxItem__user}>{user}</p>
				<p className={styles.boxItem__date}>{date}</p>
			</div>
		);
	}
}

BoxItem.propTypes = {
	countCards: PropTypes.number,
	title: PropTypes.string,
	subtitle: PropTypes.string,
	user: PropTypes.string,
	date: PropTypes.string,
	goToPage: PropTypes.func,
	id: PropTypes.number,
	type: PropTypes.string,
	fetchDeleteBox: PropTypes.func,
};

export default BoxItem;
