import React, {Component} from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import BoxPage from './BoxPage';
import BoxEmptyPage from './BoxEmptyPage';

class BoxesView extends Component {
	// shouldComponentUpdate(nextProps) {
	// 	if(this.props.boxes.length &&
	// 			this.props.boxes.length === nextProps.boxes.length &&
	// 				this.props.boxes[0].id === nextProps.boxes[0].id &&
	// 					this.props.history.location.state.type === nextProps.history.location.state.type){
	// 		return false;
	// 	}
	// 	return true;
	// }

	componentDidMount() {
		this.props.fetchGetBoxes(queryString.stringify(this.props.history.location.state));
	}

	componentDidUpdate() {
		this.props.fetchGetBoxes(queryString.stringify(this.props.history.location.state));
	}

	render() {
		const { boxes, history, fetchDeleteBox	} = this.props;
		if(boxes) {
			return (
				<BoxPage fetchDeleteBox={fetchDeleteBox} goToPage={this.props.history.push} boxes={boxes}/>
			);
		}
		return (
			<BoxEmptyPage history={history}/>
		);
	}
}

BoxesView.propTypes = {
	boxes: PropTypes.array,
	history: PropTypes.object,
	fetchGetBoxes: PropTypes.func,
	fetchDeleteBox: PropTypes.func,
};

export default BoxesView;