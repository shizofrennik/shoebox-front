import React from 'react';
import PropTypes from 'prop-types';
import BoxItem from './BoxItem';
import Button from '../../../../components/Button/Button';
import ButtonArrow from '../../../../components/Button/ButtonArrow';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import styles from './Box.module.scss';

function BoxPage({ boxes, goToPage, fetchDeleteBox }) {
	return (
		<div className={styles.containerContent}>
			<TitleSection text="Boxes" />
			<div className={styles.row}>
				<ButtonArrow text="+ Add box" />
				<Button transparentShadow text="Deal box" handleClick={() => goToPage('/newboxdeal')}/>
				<Button transparentShadow text="Company Box" handleClick={() => goToPage('/newboxcompany')}/>
				<Button transparentShadow text="Box Contact" handleClick={() => goToPage('/newboxcontact')}/>
			</div>
			<div className={styles.boxContainer}>
				{
					boxes.map(item => (
						<BoxItem
							fetchDeleteBox={fetchDeleteBox}
							goToPage={goToPage}
							id={item.id}
							countCards={item.total_cards}
							title={item.box_name}
							subtitle={
								item.box_company_id ? 'companies' :
									item.box_deal_id ? 'deals' :
										item.box_contact_id ? 'contacts'
											: null}
							type={
								item.box_company_id ? 'company' :
									item.box_deal_id ? 'deal' :
										item.box_contact_id ? 'contact'
											: null}
							user={`${item.user.first_name} ${item.user.second_name}`}
							date={item.created_at}
							key={item.id}
						/>
					))
				}
			</div>
		</div>

	);
}

BoxPage.propTypes = {
	boxes: PropTypes.array,
	goToPage: PropTypes.func,
	fetchDeleteBox: PropTypes.func,
};

export default BoxPage;
