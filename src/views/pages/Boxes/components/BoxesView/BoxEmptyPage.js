import React, { Component } from 'react';
import Button from '../../../../components/Button/Button';
import ButtonArrow from '../../../../components/Button/ButtonArrow';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import styles from './Box.module.scss';

class BoxEmptyPage extends Component {

	goToPage = (path) => {
		this.props.history.push(path);
	};

	render() {
		return (
			<div className={styles.containerContent}>
				<TitleSection text="Box" />
				<div className={styles.rows}>
					<div>
						<p>You do not have any box</p>
						<div className={styles.row}>
							<Button text="+ Add box" />
						</div>
						<div className={styles.row}>
							<ButtonArrow text="+ Add box" />
							<Button transparentShadow text="Deal box" handleClick={() => this.goToPage('/boxes/newboxdeal')}/>
							<Button transparentShadow text="Company Box" handleClick={() => this.goToPage('/boxes/newboxcompany')}/>
							<Button transparentShadow text="Box Contact" handleClick={() => this.goToPage('/boxes/newboxcontact')}/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default BoxEmptyPage;
