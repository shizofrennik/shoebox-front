import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import CreateAccount from '../components/CreateAccount';
import { authActions } from '../../../../state/ducks/auth';

const { fetchAuth } = authActions;

const mapStateToProps = (state) => {
	return ({
		user: state.auth.user,
		isAuthenticated: state.auth.isAuthenticated
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchAuth
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateAccount);
