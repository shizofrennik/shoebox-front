import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';
import styles from './CreateAccount.module.scss';
import Input from '../../../components/Form/Input/Input';
import CheckboxSingle from '../../../components/Form/Checkbox/CheckboxSingle';
import Button from '../../../components/Button/Button';

class CreateAccount extends Component {
	state = {
		createAccountIsDisabled: true,
	}

	toggleCreateAccount = () => {
		this.setState({createAccountIsDisabled: !this.state.createAccountIsDisabled});
	}

	render() {
		const { user, fetchAuth } = this.props;
		const { createAccountIsDisabled } = this.state;
		return (
			<>
				{/* {isAuthenticated ? <Redirect to="/"/> : null } */}
				<Form
					onSubmit={fetchAuth}
					validate={values => {
						const errors = {};
						if (!values.first_name) {
							errors.first_name = "Required";
						}
						if (!values.last_name) {
							errors.last_name = "Required";
						}
						if (!values.company_name) {
							errors.company_name = "Required";
						}
						return errors;
					}}
					render={({handleSubmit, form}) => (
					<form onSubmit={handleSubmit} className={styles.form} >
						<h1 className={styles.head}>Create an account</h1>
						<h2 className={styles.subHead}>Personal Information</h2>
						<div className={styles.row}>
							<Field name="first_name" labelText="First Name" placeholder="For example, John" component={Input}/>
						</div>
						<div className={styles.row}>
							<Field name="last_name" labelText="Last Name" placeholder="For example, Roberts" component={Input}/>
						</div>
						<h2 className={styles.subHead}>Company Information</h2>
						<div className={styles.row}>
							<Field name="company_name" labelText="Company Name" placeholder="For example, Apple" component={Input}/>
						</div>
						<div className={styles.row}>
							<Field name="company_email" labelText="Company E-mail" placeholder="For example, Company@mail.com" component={Input}/>
						</div>
						<div className={styles.row}>
							<Field name="company_phone" labelText="Company Phone" placeholder="For example, +234 433 54 54" component={Input}/>
						</div>
						<div className={styles.wideRow}>
							<CheckboxSingle text="I agree to subscription terms and conditions" handleChange={this.toggleCreateAccount}/>
						</div>
						<div className={styles.bigRow}>
							<Button
								full
								text="Create account"
								disabled={createAccountIsDisabled}
								type="submit" />
						</div>
						<div className={styles.row}>
							<Button full transparentBorder text="Cancel" />
						</div>
						{user.name && <p>User: {user.name}</p>}
					</form>
				) } />
			</>
		);
	}
}

CreateAccount.propTypes = {
	user: PropTypes.object,
	fetchAuth: PropTypes.func,
	isAuthenticated: PropTypes.bool,
};

export default CreateAccount;
