import React from 'react';
import PropTypes from 'prop-types';
import styles from './AuthView.module.css';

const AuthView = ( { user, fetchAuth } ) => {
	return (
		<div className={styles.container}>
			AUTH PAGE!
			{user.name && <p>User: {user.name}</p>}
			<p>
				<button onClick={fetchAuth}>Fetch user action</button>
			</p>
		</div>
	);
};

AuthView.propTypes = {
	user: PropTypes.object,
	fetchAuth: PropTypes.func,
};

export default AuthView;