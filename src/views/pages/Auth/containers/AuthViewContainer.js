// import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import AuthView from '../components/AuthView';
import { authActions } from '../../../../state/ducks/auth';

const { fetchAuth } = authActions;

const mapStateToProps = (state) => {
	return ({
		user: state.auth.user
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchAuth
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthView);