import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import Button from '../../../../components/Button/Button';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import Input from '../../../../components/Form/Input/Input';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import CheckboxSingle from '../../../../components/Form/Checkbox/CheckboxSingle';
import RadioBtn from '../../../../components/Form/RadioBtn/RadioBtn';
import RadioBtnGroup from '../../../../components/Form/RadioBtnGroup/RadioBtnGroup';
import DatePick from '../../../../components/Form/DatePick/DatePick';
import RangeCheckbox from '../../../../components/Form/Range/RangeCheckbox';
import Line from '../../../../components/Line/Line';
import styles from './EditBoxDealView.module.scss';

class EditBoxDealView extends Component {
	state = {
		updatesIsActive: this.props.boxData.box_deal.updates ? true : false,
	}

	onSubmit = values => {
		return new Promise(resolve => {
			setTimeout(() => {
				console.log(JSON.stringify(values, 0, 2));
				resolve({
					box_name: ['require'],
				});
			}, 200);
		});
	};

	toggleUpdatesCheckboxes = () => {
		this.setState({updatesIsActive: !this.state.updatesIsActive})
	}

	render() {
		const { updatesIsActive } = this.state;
		const { boxData } = this.props;
		return (
			<>
				<Form
					initialValues={boxData.box_deal}
					onSubmit={this.onSubmit}
					validate={values => {
						const errors = {};
						if (!values.name) {
							errors.name = 'Required';
						}
						return errors;
					}}
				render={ ( {handleSubmit} ) => (
					<div className={styles.containerBoxForm}>
						<TitleSection text="Edit Box for Deal" />
						<form onSubmit={handleSubmit} className={styles.formNewBox}>
							<h3>Box</h3>
							<div className={styles.rowForm}>
								<Field labelText="Box Name" name="name" placeholder="For example, Hot Prospects" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="State" placeholder="" name="state" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="City" placeholder="" name="city" component={Input}/>
							</div>
							<h3 style={{ marginTop: '54px' }}>Add card to the Box</h3>
							<p className={styles.headText}>Created date</p>
							<div className={[`${styles.rowForm} ${styles.rowFlex}`]}>
								<Field labelText="From" icon={true} name="created_date_from" component={DatePick}/>
								<span style={{ marginBottom: '8px' }}>-</span>
								<Field labelText="To" icon={true} name="created_date_to" component={DatePick}/>
							</div>
							<div className={[`${styles.rowForm} ${styles.wrapperRadio}`]}>
								<Field type="radio" value="day" text="One day ago" name="deal_date" component={RadioBtnGroup}/>
								<Field type="radio" value="week" text="This week" name="deal_date" component={RadioBtnGroup}/>
								<Field type="radio" value="month" text="This month" name="deal_date" component={RadioBtnGroup}/>
								<Field type="radio" value="quarter" text="This quarter" name="deal_date" component={RadioBtnGroup}/>
								<Field type="radio" value="year" text="This year" name="deal_date" component={RadioBtnGroup}/>
							</div>
							<h4 style={{ marginTop: '38px' }}>Additional settings</h4>
							<div className={[`${styles.rowForm} ${styles.rowFlex}`]}>
								<div className={styles.flGrow} style={{ marginRight: '26px' }}>
									<Field
										labelText="Stage"
										placeholder="Offer"
										name="stage"
										component={Dropdown}
										options={optionsStage}
										// parse={val => val && val.value}
										// format={val => val && optionsStage.find(o => o.value === val.value)}
									/>
								</div>
								<Field type="checkbox" text="Viewed a demonstration" name="viewed" component={Checkbox}/>
							</div>
							<div className={styles.rowForm}>
								<Field
									labelText="Source"
									placeholder="Trade show"
									name="source"
									component={Dropdown}
									options={optionsSource}
									// parse={val => val && val.value}
									// format={val => val && optionsSource.find(o => o.value === val.value)}
								/>
							</div>
							<p className={styles.headText}>Filters</p>
							<div className={[`${styles.rowForm} ${styles.rowFlex}`]}>
								<div className={styles.wrapperInput}>
									<Field labelText="Value from" placeholder="$200" name="value_from" component={Input}/>
								</div>
								<div className={styles.wrapperInput}>
									<Field labelText="To" placeholder="$500" name="value_to" component={Input}/>
								</div>
							</div>
							<div className={[`${styles.rowForm} ${styles.rowFlex}`]}>
								<Field labelText="Created Date from" name="filter_created_date_from" component={DatePick}/>
								<Field labelText="To" name="filter_created_date_to" component={DatePick}/>
							</div>
							<div className={styles.rowForm}>
								<Field
									labelText="Contact"
									placeholder="Smith Merch"
									name="contact"
									component={Dropdown}
									options={optionsContact}
									// parse={val => val && val.value}
									// format={val => val && optionsContact.find(option => option.value === val.value)}
								/>
							</div>

							<h4 style={{ marginTop: '50px' }}>Activity</h4>
							<p className={styles.headText}>Last activity date</p>
							<div className={[`${styles.rowForm} ${styles.rowFlex}`]}>
								<Field labelText="From" icon={true} name="activity_date_from" component={DatePick}/>
								<span style={{ marginBottom: '8px' }}>-</span>
								<Field labelText="To" icon={true} name="activity_date_to" component={DatePick}/>
							</div>
							<div className={styles.rowForm}>
								<Field
									labelText="Type of activity"
									placeholder="Text sent"
									name="activity_type"
									component={Dropdown}
									options={optionsActivity}
									// parse={val => val && val.value}
									// format={val => val && optionsActivity.find(option => option.value === val.value)}
								/>
							</div>
							<p className={styles.headText}>Choose goals:</p>
							<div className={styles.rowForm}>
								<Field
									text="Note added"
									name="percent_goal"
									defaultValue={20}
									component={RangeCheckbox}
									valueCheckbox={boxData.box_deal.percent_goal ? true : false}
								/>
							</div>
							<Field type="checkbox" text="Call" value="Call" name="goal" component={Checkbox}/>
							<Field type="checkbox" text="Quote approved" value="Quote approved" name="goal" component={Checkbox}/>
							<h3 style={{ marginTop: '36px' }}>Created by</h3>
							<div className={styles.rowForm}>
								<Field
									labelText=""
									placeholder="Smith Merch"
									name="user_id"
									component={Dropdown}
									options={optionsCreators}
									// parse={val => val && val.value}
									// format={val => val && optionsCreators.find(option => option.value === val.value)}
								/>
							</div>
							<Line style={{ marginTop: '30px', marginBottom: '10px' }} />
							<div className={styles.rowCards}>
								<p className={styles.rowCards__text}>
									Cards <span>{boxData.box_deal.cards.length}</span>
								</p>
								<p className={styles.rowCards__all}>View all cards</p>
							</div>
							<h4>Assign the box</h4>
							<div className={styles.rowForm}>
								<Field labelText="To" placeholder="" name="assigned_to" component={Input}/>
							</div>
							<div className={[`${styles.rowTextarea} ${styles.rowForm}`]}>
								<Field labelText="Description of assignment" name="description_of_assignment" placeholder="" component={Textarea}/>
							</div>

							<CheckboxSingle value={updatesIsActive} name="updates" text="Get updates:" handleChange={this.toggleUpdatesCheckboxes}/>
							<div className={[`${styles.rowRadio} ${styles.rowForm}`]}>
								<Field
									type="radio"
									name="updates"
									value="daily"
									text="Daily"
									disabled={!updatesIsActive}
									component={RadioBtn}
								/>
								<Field
									type="radio"
									name="updates"
									value="weekly"
									text="Weekly"
									disabled={!updatesIsActive}
									component={RadioBtn}/>
								<Field
									type="radio"
									name="updates"
									value="happens"
									text="As it happens"
									disabled={!updatesIsActive}
									component={RadioBtn}/>
							</div>
							<Button text="Save Changes" full type="submit"/>
						</form>
					</div>
				)} />
			</>
		);
	}
}

const optionsContact = [
	{ value: 1, label: 'Contact-1' },
	{ value: 2, label: 'Contact-2' },
	{ value: 3, label: 'Contact-3' },
	{ value: 4, label: 'Contact-4' },
];

const optionsStage = [
	{ value: 1, label: 'Stage-1' },
	{ value: 2, label: 'Stage-2' },
	{ value: 3, label: 'Stage-3' },
	{ value: 4, label: 'Stage-4' },
];

const optionsActivity = [
	{ value: 1, label: 'Activity-1' },
	{ value: 2, label: 'Activity-2' },
	{ value: 3, label: 'Activity-3' },
	{ value: 4, label: 'Activity-4' },
];

const optionsSource = [
	{ value: 1, label: 'Source-1' },
	{ value: 2, label: 'Source-2' },
	{ value: 3, label: 'Source-3' },
	{ value: 4, label: 'Source-4' },
];

const optionsCreators = [
	{ value: 1, label: 'Creator-1' },
	{ value: 2, label: 'Creator-2' },
	{ value: 3, label: 'Creator-3' },
	{ value: 4, label: 'Creator-4' },
];

export default EditBoxDealView;
