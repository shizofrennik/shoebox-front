import React, { Component } from 'react';
import { Form, Field } from 'react-final-form';
import Button from '../../../../components/Button/Button';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import Input from '../../../../components/Form/Input/Input';
import InputSingle from '../../../../components/Form/Input/InputSingle';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import Textarea from '../../../../components/Form/Textarea/Textarea';
import Checkbox from '../../../../components/Form/Checkbox/Checkbox';
import CheckboxSingle from '../../../../components/Form/Checkbox/CheckboxSingle';
import RadioBtn from '../../../../components/Form/RadioBtn/RadioBtn';
import Line from '../../../../components/Line/Line';
import styles from './EditBoxContactView.module.scss';

class EditBoxContactView extends Component {
	state = {
		updatesIsActive: this.props.boxData.box_contact.updates ? true : false,
	}

	onSubmit = values => {
		return new Promise(resolve => {
			setTimeout(() => {
				console.log(JSON.stringify(values, 0, 2));
				resolve({
					box_name: ['require'],
				});
			}, 200);
		});
	};

	toggleUpdatesCheckboxes = () => {
		this.setState({updatesIsActive: !this.state.updatesIsActive})
	}

	render() {
		const { updatesIsActive } = this.state;
		const { boxData } = this.props;
		return (
			<>
				<Form
					initialValues={boxData.box_contact}
					onSubmit={this.onSubmit}
					validate={values => {
						const errors = {};
						if (!values.name) {
							errors.name = 'Required';
						}
						return errors;
					}}
				render={ ( {handleSubmit} ) => (
					<div className={styles.containerBoxForm}>
						<TitleSection text="Edit Box for Contact" />
						<form onSubmit={handleSubmit} className={styles.formNewBox}>
							<h3>Box</h3>
							<div className={styles.rowForm}>
								<Field labelText="Box Name" name="name" placeholder="For example, Hot Prospects" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="First Name" placeholder="" name="first_name" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="Last Name" placeholder="" name="last_name" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="Company" placeholder="" name="company" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field labelText="Job Title" placeholder="" name="job_title" component={Input}/>
							</div>
							<div className={styles.rowForm}>
								<Field
									labelText="Source"
									placeholder="International Kitchen and Bath Trade Show 2019"
									name="source"
									component={Dropdown}
									options={optionsSource}
									// parse={val => val && val.value}
									// format={val => val && optionsSource.find(o => o.value === val.value)}
								/>
							</div>
							<div className={[`${styles.rowForm} ${styles.dFlex}`]}>
								<InputSingle labelText="Create New Source" placeholder="New Source" />
								<Button text="Add + " style={{height: '60%', marginLeft: '11px'}}/>
							</div>
							<div className={styles.rowForm}>
								<Field
									isMulti
									linksIsActive
									labelText="Associated deal(s)"
									placeholder="Deals"
									name="deals"
									component={Dropdown}
									options={optionsDeals}
									// parse={val => val && val.map(item => item.value)}
									// format={val => val && val.map(item => optionsDeals.find(o => o.value === item))}
								/>
							</div>
							<div className={styles.rowForm}>
								<Field
									labelText="Industry"
									placeholder="Customized list"
									name="industry"
									component={Dropdown}
									options={optionsIndustry}
									// parse={val => val && val.value}
									// format={val => val && optionsIndustry.find(option => option.value === val.value)}
								/>
							</div>
								<Field
									type="checkbox"
									text="Viewed a demonstration"
									name="viewed"
									component={Checkbox}/>
								<Line style={{ marginTop: '30px', marginBottom: '10px' }} />
							<div className={styles.rowCards}>
								<p>
										Cards <span>{boxData.box_contact.cards.length}</span>
								</p>
								<p className={styles.rowCards__all}>View all cards</p>
							</div>
							<h4>Assign the box</h4>
							<div className={styles.rowForm}>
								<Field labelText="To" placeholder="" name="assigned_to" component={Input}/>
							</div>
							<div className={[`${styles.rowTextarea} ${styles.rowForm}`]}>
								<Field labelText="Description of assignment" name="description_of_assignment" placeholder="" component={Textarea}/>
							</div>
							<CheckboxSingle value={updatesIsActive} name="updates" text="Get updates:" handleChange={this.toggleUpdatesCheckboxes}/>
							<div className={[`${styles.rowRadio} ${styles.rowForm}`]}>
								<Field
									type="radio"
									name="updates"
									value="daily"
									text="Daily"
									disabled={!updatesIsActive}
									component={RadioBtn}
								/>
								<Field
									type="radio"
									name="updates"
									value="weekly"
									text="Weekly"
									disabled={!updatesIsActive}
									component={RadioBtn}/>
								<Field
									type="radio"
									name="updates"
									value="happens"
									text="As it happens"
									disabled={!updatesIsActive}
									component={RadioBtn}/>
							</div>
							<Button text="Save Changes" full type="submit"/>
						</form>
					</div>
				)} />
			</>
		);
	}
}

const optionsDeals = [
	{ value: 1, label: 'Deal-1' },
	{ value: 2, label: 'Deal-2' },
	{ value: 3, label: 'Deal-3' },
	{ value: 4, label: 'Deal-4' },
];

const optionsSource = [
	{ value: 1, label: 'Source-1' },
	{ value: 2, label: 'Source-2' },
	{ value: 3, label: 'Source-3' },
	{ value: 4, label: 'Source-4' },
];

const optionsIndustry = [
	{ value: 1, label: 'Industry-1' },
	{ value: 2, label: 'Industry-2' },
	{ value: 3, label: 'Industry-3' },
	{ value: 4, label: 'Industry-4' },
];

export default EditBoxContactView;
