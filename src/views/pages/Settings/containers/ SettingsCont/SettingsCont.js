import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { authActions } from '../../../../../state/ducks/auth';
import SettingsView from '../../components/SettingsView';
// import { boxSelectors } from '../../../../../state/ducks/box';

const { fetchEditUser } = authActions;
// const {  } = boxSelectors;

const mapStateToProps = (state) => {
	return ({
		userData: state.auth.user
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		fetchEditUser,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsView);
