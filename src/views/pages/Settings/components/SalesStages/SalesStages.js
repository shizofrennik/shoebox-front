import React, { Component } from 'react';
import styles from './SalesStages.module.scss';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import InputSingle from '../../../../components/Form/Input/InputSingle';
import Button from '../../../../components/Button/Button';

class SalesStages extends Component {
	state = {
		stage: ""
	}

	handleInputStage = (e) => {
		this.setState({stage: e.target.value})
	}
	// eslint-disable-next-line class-methods-use-this
	render() {
		let { stage, fetchEditUser } = this.state;
		return (
		<>
			<div style={{ marginTop: '37px' }}>
				<TitleSection text="Sales Stages" />
			</div>
			<div className={[`${styles.contFlex} ${styles.contFlex_W40}`]}>
				<InputSingle
					onChange={this.handleInputStage}
					value={stage}
					name="stage"
					labelText="Stage Name"
					placeholder="For example, Unqualified Lead "
				/>
				<Button handleClick={() => fetchEditUser({stage})}  disabled={!stage} text="+ Add new stage" style={{ marginLeft: '6px', height: '32px' }} />
			</div>
			<ol className={styles.list}>
				<li className={styles.listItem}>Unqualified Lead</li>
				<li className={styles.listItem}>Qualified Lead</li>
				<li className={styles.listItem}>Presentation ready</li>
				<li className={styles.listItem}>Presentation Complete</li>
				<li className={styles.listItem}>Initial Quote Delivered</li>
				<li className={styles.listItem}>Initial Evaluation Complete</li>
				<li className={styles.listItem}>Approved Final Quote/verbal</li>
				<li className={styles.listItem}>Won</li>
			</ol>
		</>
		);
	}
}

export default SalesStages;
