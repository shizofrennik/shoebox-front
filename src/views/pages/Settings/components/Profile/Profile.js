import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Field } from 'react-final-form';
import Button from '../../../../components/Button/Button';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import InfoRowMutable from '../../../../components/InfoRowMutable/InfoRowMutable';
import InputFileLoader from '../../../../components/Form/InputFileLoader/InputFileLoader';
import Dropdown from '../../../../components/Form/Dropdown/Dropdown';
import styles from './Profile.module.scss';

class Profile extends Component {
	// eslint-disable-next-line class-methods-use-this
	render() {
		const { userData, fetchEditUser } = this.props;
		return (
			<>
				<div style={{ marginTop: '37px' }}>
					<TitleSection text="Profile" />
				</div>
					<Form
						initialValues={userData}
						onSubmit={(values) => {
							console.log(values);
							fetchEditUser(values);
						}}
						validate={values => {
							const errors = {};
							if (!values.first_name) {
								errors.first_name = 'Required';
							}
							return errors;
						}}
						render={({handleSubmit, submitting, pristine}) => (
							<form onSubmit={handleSubmit} >
								<div className={styles.contFlex}>
									<div className={styles.column}>
										<p className={styles.subHead}>Personal Information</p>
										<Field labelText="First Name" name="first_name" component={InfoRowMutable}/>
										<Field labelText="Last Name" name="last_name" component={InfoRowMutable}/>
										<Field labelText="Date of Birth" name="day_of_birth" component={InfoRowMutable}/>
										<Field labelText="job Title" name="job_title" component={InfoRowMutable}/>
									</div>
									<div className={styles.column}>
										<p className={styles.subHead}>Contact Information</p>
										<Field labelText="E-mail address" name="email" component={InfoRowMutable}/>
										<Field labelText="Address-1" name="address_1" component={InfoRowMutable}/>
										<Field labelText="Address-2" name="address_2" component={InfoRowMutable}/>
										<Field
											labelText="State"
											name="state"
											placeholder="Choose State"
											component={Dropdown}
											options={[
												{label: 'Florida', value: '99'},
												{label: 'Montana', value: '98'},
												{label: 'North Carolina', value: '97'},
												{label: 'Texas', value: '96'},
											]}
											stateName
										/>
										<Field labelText="City" name="city" component={InfoRowMutable}/>
										<Field labelText="ZIP Code" name="zip_code" component={InfoRowMutable}/>

									</div>
									<div className={styles.column}>
										<Field type="file" name="avatar" component={InputFileLoader}/>
										{/* <Field type="file" name="avatar" render={({input}) => <input {...input} />}/> */}
										{ submitting || pristine ? null :
											<Button
												disabled={submitting || pristine}
												type="submit"
												style={{ marginTop: '130px', width: '90%' }}
												text="Save changes"
											/>}
									</div>
								</div>
							</form>
						)}
					>
					</Form>
			</>
		);
	}
}

Profile.propTypes = {
	userData: PropTypes.object,
	fetchEditUser: PropTypes.func,
};

export default Profile;
