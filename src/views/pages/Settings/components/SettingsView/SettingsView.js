import React, { Component } from 'react';
import styles from './SettingsView.module.scss';
import TitleSection from '../../../../components/TitleSection/TitleSection';
import RadioBtnGroupNav from '../../../../components/Form/RadioBtnGroup/RadioBtnGroupNav';
// import eclipse from '../../../../../assets/images/eclipse.png';
import Profile from '../Profile';
import SalesStages from '../SalesStages';

class SettingsView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			screen: 'stages',
		};
	}

	changeScreen = e => {
		this.setState({ screen: e.target.value });
	}
	// eslint-disable-next-line class-methods-use-this
	render() {
		let { screen }  = this.state;
		const { userData, fetchEditUser } = this.props;
		return (
			<div className={styles.containerContent}>
				<div style={{ marginLeft: '44px' }}>
					<TitleSection text="Settings" />
				</div>
				<div className={styles.containerProfile}>
					<div className={styles.contFlex}>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="stages"
							text="Sales Stages"
							handleChange={this.changeScreen}
							checked={ screen === 'stages' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="phrases"
							text="Preset Phrases"
							handleChange={this.changeScreen}
							checked={ screen === 'phrases' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="users"
							text="Users"
							handleChange={this.changeScreen}
							checked={ screen === 'users' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="fields"
							text="Custom Fields"
							handleChange={this.changeScreen}
							checked={ screen === 'fields' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="analitics"
							text="Analitics"
							handleChange={this.changeScreen}
							checked={ screen === 'analitics' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="profile"
							text="Profile"
							handleChange={this.changeScreen}
							checked={ screen === 'profile' }
						/>
						<RadioBtnGroupNav
							style={{ width: '130px' }}
							name="settings"
							value="data"
							text="Data Importer"
							handleChange={this.changeScreen}
							checked={ screen === 'data' }
						/>
						<RadioBtnGroupNav
							name="settings"
							value="bot"
							text="Bot"
							handleChange={this.changeScreen}
							checked={ screen === 'bot' }
						/>
					</div>
					{ screen === 'stages' ? <SalesStages fetchEditUser={fetchEditUser} userData={userData}/> :
						screen === 'profile' ? <Profile  fetchEditUser={fetchEditUser} userData={userData}/> : null }
				</div>
			</div>
		);
	}
}

export default SettingsView;
