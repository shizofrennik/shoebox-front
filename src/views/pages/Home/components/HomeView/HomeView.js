import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './HomeView.module.css';

class HomeView extends Component {

	componentDidMount() {
		this.props.getCards();
	}

	render() {
		return (
			<div className={styles.container}> HOME PAGE! </div>
		);
	}
}

HomeView.propTypes = {
	getCards: PropTypes.func,
};

export default HomeView;
