import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HomeView from '../../components/HomeView';
import { cardsActions } from '../../../../../state/ducks/cards';

const { getCards } = cardsActions;

const mapStateToProps = (state) => {
	return ({
		cards: state.cards,
	});
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		getCards
	}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);