import withAuthentication from '../views/hocs/withAuthentication';
import { Home, Boxes, CreateBoxDeal, CreateBoxContact, CreateBoxCompany,
	EditBoxDeal, EditBoxCompany, EditBoxContact, Settings, BoxContent,
	LogIn } from '../views/pages';

const routes = [
	{
		path: '/',
		component: withAuthentication(Home),
		exact: true,
	},
	{
		path: '/auth',
		component: LogIn,
		exact: true,
	},
	{
		path: '/boxes',
		component: withAuthentication(Boxes),
		exact: true,
	},
	{
		path: '/boxes/:id',
		component: withAuthentication(BoxContent),
		exact: true,
	},
	{
		path: '/newboxdeal',
		component: withAuthentication(CreateBoxDeal),
		exact: true,
	},
	{
		path: '/newboxcontact',
		component: withAuthentication(CreateBoxContact),
		exact: true,
	},
	{
		path: '/newboxcompany',
		component: withAuthentication(CreateBoxCompany),
		exact: true,
	},
	{
		path: '/editboxdeal',
		component: withAuthentication(EditBoxDeal),
		exact: true,
	},
	{
		path: '/editboxcontact',
		component: withAuthentication(EditBoxContact),
		exact: true,
	},
	{
		path: '/editboxcompany',
		component: withAuthentication(EditBoxCompany),
		exact: true,
	},
	{
		path: '/settings',
		component: withAuthentication(Settings),
		exact: true,
	},
];

export default routes;
