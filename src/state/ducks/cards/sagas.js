import * as types from './types';
import * as actions from './actions';
import * as fakeData from '../../../fakeData';
import {
	put,
	takeLatest,
	// call
} from 'redux-saga/effects';

// const fetch = (url, method, options) => {
// 	return new Promise((resolve) => {
// 		setTimeout(() => {
// 			method === 'GET' ?
// 				url === 'https://api.com/cards?type=company' ? resolve(fakeData.companies) :
// 					url === 'https://api.com/cards?type=deal' ? resolve(fakeData.deals) :
// 						url === 'https://api.com/cards?type=contact' ? resolve(fakeData.contacts) :	resolve([])
// 				: resolve(options);
// 		}, 200);
// 	});
// };

function* getCards() {
	try {
		yield put({type: types.FETCH_GET_CARDS_START});
		// const deals = call(fetch, 'https://api.com/cards?type=deal', 'GET');
		yield put(actions.saveDeals(fakeData.dealsForCards));

		// const companies = call(fetch, 'https://api.com/cards?type=company', 'GET');
		yield put(actions.saveCompanies(fakeData.companiesForCards));

		// const contacts = call(fetch, 'https://api.com/cards?type=contact', 'GET');
		yield put(actions.saveContacts(fakeData.contactsForCards));
		yield put({type: types.FETCH_GET_CARDS_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_GET_CARDS_FAILED});
	}
}

export default [
	takeLatest(types.FETCH_GET_CARDS, getCards),
];