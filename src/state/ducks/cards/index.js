import reducer from './reducers';
import * as actions from './actions'; //actions
import sagas from './sagas'; //sagas
import * as selectors from './selectors';

export const cardsActions = actions;
export const cardsSagas = sagas;
export const cardsSelectors = selectors;
export const cardsReducer = reducer;

// export default {
// 	authActions,
// 	authSagas,
// 	authSelectors,
// 	authReducer
// };

// export default reducer;