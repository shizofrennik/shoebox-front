export const FETCH_GET_CARDS = 'cards/FETCH_GET_CARDS';
export const FETCH_GET_CARDS_START = 'cards/FETCH_GET_CARDS_START';
export const FETCH_GET_CARDS_COMPLETED = 'cards/FETCH_GET_CARDS_COMPLETED';
export const FETCH_GET_CARDS_FAILED = 'cards/FETCH_GET_CARDS_FAILED';

export const SAVE_DEALS = 'cards/SAVE_DEALS';
export const SAVE_COMPANIES = 'cards/SAVE_COMPANIES';
export const SAVE_CONTACTS = 'cards/SAVE_CONTACTS';
