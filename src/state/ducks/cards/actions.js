import * as types from './types';

export const getCards = () => ({
	type: types.FETCH_GET_CARDS,
});

export const saveDeals = (deals) => ({
	type: types.SAVE_DEALS,
	payload: deals,
});

export const saveCompanies = (companies) => ({
	type: types.SAVE_COMPANIES,
	payload: companies,
});

export const saveContacts = (contacts) => ({
	type: types.SAVE_CONTACTS,
	payload: contacts,
});


