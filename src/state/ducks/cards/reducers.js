import * as types from './types';
import { createReducer } from '../../utils';

export default createReducer({
	deals: [],
	contacts: [],
	companies: [],
})(
	{
		[types.SAVE_DEALS]: (state, action) => ({ ...state, deals: action.payload}),
		[types.SAVE_COMPANIES]: (state, action) => ({ ...state, companies: action.payload}),
		[types.SAVE_CONTACTS]: (state, action) => ({ ...state, contacts: action.payload})
	}
);
