import * as types from './types';
import * as actions from './actions';
import * as fakeData from '../../../fakeData';
import {put, takeLatest, call} from 'redux-saga/effects';

const fetch = (url, method, options) => {
	return new Promise((resolve) => {
		setTimeout(() => resolve(options), 2000);
	});
};

function* authFlow(action) {
	try {
		const credentials = action.payload;
		/* eslint-disable */
    console.log('creds: ', credentials);
    /* eslint-enable */
		yield put({type: types.FETCH_AUTH_START});
		yield put(actions.setUser(fakeData.user));
		yield put(actions.changeAuthStatus(true));
		//fetching data here
		yield put({type: types.FETCH_AUTH_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_AUTH_FAILED});
	}
}

function* createAccount(action) {
	try {
		yield put({type: types.FETCH_CREATE_ACCOUNT_START});
		yield call(fetch, 'https://api.com/post/boxes','POST', action.payload);
		yield put(actions.changeRegistrationStatus('successful'));
		yield put({type: types.FETCH_AUTH_COMPLETED});
	} catch(error) {
		yield put(actions.changeRegistrationStatus('failed'));
		yield put({type: types.FETCH_AUTH_FAILED});
	}
}

function* editUser(action) {
	try {
		yield put({type: types.FETCH_EDIT_USER_START});
		const updatedUserData = yield call(fetch, 'https://api.com/post/user','POST', action.payload);
		yield put(actions.setUser(updatedUserData));
		yield put({type: types.FETCH_EDIT_USER_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_EDIT_USER_FAILED});
	}
}

export default [
	takeLatest(types.FETCH_AUTH, authFlow),
	takeLatest(types.FETCH_CREATE_ACCOUNT, createAccount),
	takeLatest(types.FETCH_EDIT_USER, editUser),
];