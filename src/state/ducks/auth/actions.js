import * as types from './types';

export const fetchAuth = () => ({
	type: types.FETCH_AUTH
});

export const fetchCreateAccount = (registrationData) => ({
	type: types.FETCH_CREATE_ACCOUNT,
	payload: registrationData,
});

export const fetchEditUser = (userOption) => ({
	type: types.FETCH_EDIT_USER,
	payload: userOption,
});

export const setUser = (user) => ({
	type: types.SET_USER,
	payload: user
});

export const changeAuthStatus = (bool) => ({
	type: types.IS_AUTHENTICATED,
	payload: bool
});

export const changeRegistrationStatus = (status) => ({
	type: types.ACCOUNT_IS_CREATED,
	payload: status
});
