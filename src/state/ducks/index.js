import { all } from 'redux-saga/effects';
import { authSagas, authReducer } from './auth';
import { boxSagas, boxReducer } from './box';
import { cardsSagas, cardsReducer } from './cards';

export function* rootSaga() {
	yield all([
		...authSagas,
		...boxSagas,
		...cardsSagas,
	]);
}

export const reducers = {
	auth: authReducer,
	box: boxReducer,
	cards: cardsReducer,
};
