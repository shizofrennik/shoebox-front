import * as types from './types';
import * as actions from './actions';
import * as fakeData from '../../../fakeData';
import { put, takeLatest, call } from 'redux-saga/effects';

const fetch = (url, method, options) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			method === 'GET' ?
				url === 'https://api.com/boxes?type=all' ? resolve(fakeData.boxes) :
					url === 'https://api.com/boxes?type=company' ? resolve(fakeData.boxes.filter(item => item.box_company)) :
						url === 'https://api.com/boxes?type=deal' ? resolve(fakeData.boxes.filter(item => item.box_deal)) :
							url === 'https://api.com/boxes?type=contact' ? resolve(fakeData.boxes.filter(item => item.box_contact)) :	resolve([])
				: resolve(options);
		}, 200);
	});
};

function* getBoxes(action) {
	try {
		yield put({type: types.FETCH_GET_BOXES_START});
		const boxes = yield call(
			fetch,
			`https://api.com/boxes?${action.payload}`,
			'GET',
		);
		yield put(actions.saveBoxes(boxes));
		yield put({type: types.FETCH_GET_BOXES_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_GET_BOXES_FAILED, payload: error});
	}
}

function* searchBoxes(action) {
	try {
		yield put({type: types.FETCH_SEARCH_BOXES_START});
		const boxes = yield call(
			fetch,
			`https://api.com/boxes?${action.payload.queryString}`,
			'GET',
		);
		yield put(actions.saveBoxes(boxes));
		yield put({type: types.FETCH_SEARCH_BOXES_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_SEARCH_BOXES_FAILED, payload: error});
	}
}

function* addBox(action) {
	try {
		yield put({type: types.FETCH_ADD_BOX_START});
		yield call(
			fetch,
			'https://api.com/post/boxes',
			'POST',
			action.payload.options);
		yield put({type: types.FETCH_ADD_BOX_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_ADD_BOX_FAILED, payload: error});
	}
}

function* addCard(action) {
	try {
		yield put({type: types.FETCH_ADD_CARD_START});
		yield call(
			fetch,
			'https://api.com/post/cards',
			'POST',
			action.payload,
		);
		yield put({type: types.FETCH_ADD_CARD_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_ADD_CARD_FAILED, payload: error});
	}
}

function* editCard(action){
	try {
		yield put({type: types.FETCH_EDIT_CARD_START});
		yield call(
			fetch,
			'https://api.com/post/cards',
			'POST',
			action.payload,
		);
		yield put({type: types.FETCH_EDIT_CARD_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_EDIT_CARD_FAILED, payload: error});
	}
}

function* editBox(action){
	try {
		yield put({type: types.FETCH_EDIT_BOX_START});
		const boxes = yield call(
			fetch,
			'https://api.com/post/box',
			'POST',
			action.payload,
		);
		yield put(actions.saveBoxes(boxes));
		yield put({type: types.FETCH_EDIT_BOX_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_EDIT_BOX_FAILED, payload: error});
	}
}

function* deleteBox(action){
	try {
		yield put({type: types.FETCH_DELETE_BOX_START});
		const boxes = yield call(
			fetch,
			'https://api.com/post/box',
			'POST',
			action.payload,
		);
		yield put(actions.saveBoxes(boxes));
		yield put({type: types.FETCH_DELETE_BOX_COMPLETED});
	} catch(error) {
		yield put({type: types.FETCH_DELETE_BOX_FAILED, payload: error});
	}
}

export default [
	takeLatest(types.FETCH_GET_BOXES, getBoxes),
	takeLatest(types.FETCH_SEARCH_BOXES, searchBoxes),
	takeLatest(types.FETCH_ADD_BOX, addBox),
	takeLatest(types.FETCH_ADD_CARD, addCard),
	takeLatest(types.FETCH_EDIT_CARD, editCard),
	takeLatest(types.FETCH_EDIT_BOX, editBox),
	takeLatest(types.FETCH_DELETE_BOX, deleteBox),
];
