import reducer from './reducers';
import * as actions from './actions'; //actions
import sagas from './sagas'; //sagas
import * as selectors from './selectors';

export const boxActions = actions;
export const boxSagas = sagas;
export const boxSelectors = selectors;
export const boxReducer = reducer;