import * as types from './types';

export const fetchGetBoxes = (type) => ({
	type: types.FETCH_GET_BOXES,
	payload: type || ''
});

export const fetchSearchBoxes = (queryString) => ({
	type: types.FETCH_SEARCH_BOXES,
	payload: {queryString},
});


export const fetchAddBox = (options) => ({
	type: types.FETCH_ADD_BOX,
	payload: options,
});

export const saveBoxes = (boxes) => ({
	type: types.SAVE_BOXES,
	payload: boxes,
});

export const fetchAddCard = (options) => ({
	type: types.FETCH_ADD_CARD,
	payload: options,
});

export const fetchEditCard = (options) => ({
	type: types.FETCH_EDIT_CARD,
	payload: options,
});

export const fetchEditBox = (options) => ({
	type: types.FETCH_EDIT_BOX,
	payload: options,
});

export const fetchDeleteBox = (id) => ({
	type: types.FETCH_DELETE_BOX,
	payload: id,
});

