import * as types from './types';
import { createReducer } from '../../utils';

export default createReducer({boxes: []})(
	{
		[types.SAVE_BOXES]: (state, action) => ( { ...state, boxes: [...action.payload ] } ),
	}
);