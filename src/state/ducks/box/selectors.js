export const getCardsOfActiveBox = (state, id, type) => {
	const box = state.box.boxes.find(item => item.id === id );
	if(type === 'company') {
		return box.box_company.cards;
	} else if( type === 'deal' ) {
		return box.box_deal.cards;
	} else return box.box_contact.cards;
};

export const getDataOfActiveBox = (state, id) => state.box.boxes.find(item => item.id === id );